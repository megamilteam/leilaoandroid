/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package qunaticheart.com.animations;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class Loadding {

    //==============================================================================================
    //
    // ** Vars
    //
    //==============================================================================================

    private Activity mActivity;
    //
    private Dialog dialog;
    private DialogUtils.OnLoadCancelListener mOnLoadCancelListener;
    //
    private ImageButton btnCloseLoading;
    private ImageView imgLoading;
    private TextView tvLoading;
    private Button btnLoading;

    //==============================================================================================
    //
    // ** Constructor
    //
    //==============================================================================================

    public Loadding(Activity activity, DialogUtils.OnLoadCancelListener onLoadCancelListener) {
        mActivity = activity;
        mOnLoadCancelListener = onLoadCancelListener;
    }

    //==============================================================================================
    //
    // ** Utils
    //
    //==============================================================================================

    public void showLoading() {
        View view = DialogUtils.getView(mActivity, R.layout.loading_dialog);
        if (dialog == null) {
            dialog = DialogUtils.openDialogFullScreen(mActivity, view);
            DialogUtils.setLoadCancelListener(mOnLoadCancelListener);
            Animation pulse1 = AnimationUtils.loadAnimation(mActivity, R.anim.pulse);
            ((ImageView) dialog.findViewById(R.id.image1)).startAnimation(pulse1);

            Animation pulse2 = AnimationUtils.loadAnimation(mActivity, R.anim.pulse2);
            pulse2.setStartOffset(500);
            ((ImageView) dialog.findViewById(R.id.image2)).startAnimation(pulse2);

            imgLoading = view.findViewById(R.id.imgLoading);
            tvLoading = view.findViewById(R.id.tvLoading);
            btnLoading = view.findViewById(R.id.btnLoading);
            //
            btnCloseLoading = view.findViewById(R.id.btnCloseLoading);
            btnCloseLoading.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeLoading();
                }
            });
        }
        dialog.show();
    }

    public void closeLoading() {
        if (getStatusDialog()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public void setBtnClickListenner(String buttonText, View.OnClickListener onClickListener) {
        if (getStatusDialog()) {
            btnLoading.setText(buttonText);
            btnLoading.setOnClickListener(onClickListener);
        }
    }

    public void showLoaddingMsg(String msg) {
        if (getStatusDialog())
            tvLoading.setText(msg);
    }

    public boolean getStatusDialog() {
        return dialog != null;
    }

//    public static void closeLoading(final Activity activity, WSResponse response, Connection.ConnectionType wsTypeConection) {
//
//        if (dialog != null) {
//            ll_erro.setVisibility(View.VISIBLE);
//            ll_load.setVisibility(View.GONE);
//
//            switch (wsTypeConection) {
//                case WS_LOGIN:
//                    tv_erro.setText(response.getResultado());
//                    btn_erro.setVisibility(View.GONE);
//                    break;
//            }
//        }
//    }
//
//
//    public static void closeLoadingByStatus(final Activity activity, String msg, int status) {
//
//        ll_erro.setVisibility(View.VISIBLE);
//        ll_load.setVisibility(View.GONE);
//
//        switch (status) {
//            case -3:
//                tv_erro.setText("Opa! Falta validar sua conta com seu número de celular!");
//                btn_erro.setText("Validar numero de Celular");
//                btn_erro.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        FacebookAcountKitUtils.smsAtivation(activity);
//                        closeLoading();
//                    }
//                });
//                break;
//            case -5:
//                tv_erro.setText(msg);
//                btn_erro.setText("OK");
//                btn_erro.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                    }
//                });
//                break;
//        }
//
//    }


}
