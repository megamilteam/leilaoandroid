/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package qunaticheart.com.animations;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.Window;


import java.util.Objects;

@SuppressWarnings("all")
public class DialogUtils {

    private static Dialog dialog;

    public static View getView(Activity activity, int layout) {
        return activity.getLayoutInflater().inflate(layout, null);
    }

    //==============================================================================================
    //
    // ** Interface Cancel Dialog
    //
    //==============================================================================================

    private static OnLoadCancelListener mLoadCancelListener;

    public static void setLoadCancelListener(OnLoadCancelListener loadCancelListener) {
        mLoadCancelListener = loadCancelListener;
    }

    public interface OnLoadCancelListener {
        void cancelDialog();
    }

    //==============================================================================================
    //
    // ** Dialogs
    //
    //==============================================================================================
    public static Dialog openDialogFullScreen(final Activity activity, View layout) {
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(layout);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogNoAnimation;
        dialog.show();
//        dialog.setCancelable(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (mLoadCancelListener != null)
                    mLoadCancelListener.cancelDialog();
//                dialogInterface.dismiss();
            }
        });
        return dialog;
    }


    public static Dialog openDialogNoBackground(final Activity activity, View layout) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(layout);

        dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        return dialog;

    }

    public static void openDialog(final Activity activity, int layout) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final View dialogbox = activity.getLayoutInflater().inflate(layout, null);
        builder.setView(dialogbox);

        dialog = builder.create();
        dialog.show();

    }

    public static void openDialog(final Activity activity, View view) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();

    }
}
