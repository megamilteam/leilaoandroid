package maripoppis.com.socialnetwork.FaceBook;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FaceBookUtil {

    @SuppressLint("LongLogTag")
    public static void verifieFacebookHash(Activity activity) {
        Log.e("activity:", activity.getPackageName());
        Log.e("activity:", activity.getLocalClassName());
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
//                if (BuildConfig.DEBUG) {
                    Log.w("KeyHash for FacebookData:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                }
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
//            if (BuildConfig.DEBUG) {
//                Log.w("Hash for FacebookData error:", e.getCause().getMessage());
//            }
        }

    }

}
