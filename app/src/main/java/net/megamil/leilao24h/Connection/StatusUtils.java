/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Connection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;

import com.google.gson.Gson;

import net.megamil.leilao24h.Project.ProjectActivity;
import net.megamil.leilao24h.Project.ProjectDialog;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Editar_Usuario;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;

import org.json.JSONException;
import org.json.JSONObject;

import maripoppis.com.connection.Connection;
import maripoppis.com.connection.ConnectionUtils.LoadingUtils;
import maripoppis.com.connection.Model.WSResponse;

public class StatusUtils {

    public static boolean verifyStatusConnection(final Activity activity, final WSResponse response, Connection.ConnectionType connectionTypeID) {
        boolean statusOK = false;
        int status = response.getStatus();
        LoadingUtils loadingUtils = new LoadingUtils(activity);
        final Dialog dialog = loadingUtils.createDialogView();
        switch (status) {
            case -6:
                ProjectDialog.openMoreCredits(activity);
                break;
            case -4:
                loadingUtils.openDialogInvalidToken(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToolBox_Chama_Activity.Funcao_RAW_Logout(activity);
                        if(activity.hasWindowFocus())
                             dialog.dismiss();
                    }
                });
                break;
            case -5:

                loadingUtils.openDialogWithStatus(response.getResultado(), "OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Gson gson = new Gson();
                        try {
                            ProjectActivity.goPerfilActivity(activity, new JSONObject(gson.toJson(response)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(activity.hasWindowFocus())
                        dialog.dismiss();
                    }
                });

                break;
            default:
                statusOK = true;
        }
        return statusOK;
    }

}
