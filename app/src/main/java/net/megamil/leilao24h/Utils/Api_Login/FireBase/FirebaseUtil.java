package net.megamil.leilao24h.Utils.Api_Login.FireBase;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

public class FirebaseUtil {

    public static boolean verifieGoogleServisesVersion(Activity activity) {

        final int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            Log.e("", GooglePlayServicesUtil.getErrorString(status));

            // ask user to update google play services.
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, activity, 1);
            dialog.show();
            return true;
        } else {
            Log.i("", GooglePlayServicesUtil.getErrorString(status));
            // google play services is updated.
            //your code goes here...
            return false;
        }
    }

    public static String getToken(Activity activity) {
        FirebaseApp.initializeApp(activity);
        return FirebaseInstanceId.getInstance().getToken();
    }


}

