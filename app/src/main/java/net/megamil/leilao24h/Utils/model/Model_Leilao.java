package net.megamil.leilao24h.Utils.model;

import java.lang.reflect.Array;

/**
 * Created by nalmir on 12/12/2017.
 */

public class Model_Leilao {

    private int id_leilao;
    private int id_produto;
    private String nome_produto;
    private String descricao_produto;
    private String valor_minimo;
    private String lance_minimo;
    private int ingressos;
    private String data_inicio;
    private String data_fim_previsto;
    private int favorito;
    private Array imagens;

    public Model_Leilao() {
        this.id_leilao = -1;
        this.id_produto = -1;
        this.nome_produto = "sem dados";
        this.descricao_produto = "sem dados";
        this.valor_minimo = "sem dados";
        this.lance_minimo = "sem dados";
        this.ingressos = -1;
        this.data_inicio = "sem dados";
        this.data_fim_previsto = "sem dados";
        this.favorito = -1;
        this.imagens = null;
    }

    public Array getImagens() {
        return imagens;
    }

    public void setImagens(Array imagens) {
        this.imagens = imagens;
    }

    public int getId_leilao() {
        return id_leilao;
    }

    public void setId_leilao(int id_leilao) {
        this.id_leilao = id_leilao;
    }

    public int getId_produto() {
        return id_produto;
    }

    public void setId_produto(int id_produto) {
        this.id_produto = id_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getDescricao_produto() {
        return descricao_produto;
    }

    public void setDescricao_produto(String descricao_produto) {
        this.descricao_produto = descricao_produto;
    }

    public String getValor_minimo() {
        return valor_minimo;
    }

    public void setValor_minimo(String valor_minimo) {
        this.valor_minimo = valor_minimo;
    }

    public String getLance_minimo() {
        return lance_minimo;
    }

    public void setLance_minimo(String lance_minimo) {
        this.lance_minimo = lance_minimo;
    }

    public int getIngressos() {
        return ingressos;
    }

    public void setIngressos(int ingressos) {
        this.ingressos = ingressos;
    }

    public String getData_inicio() {
        return data_inicio;
    }

    public void setData_inicio(String data_inicio) {
        this.data_inicio = data_inicio;
    }

    public String getData_fim_previsto() {
        return data_fim_previsto;
    }

    public void setData_fim_previsto(String data_fim_previsto) {
        this.data_fim_previsto = data_fim_previsto;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }
}
