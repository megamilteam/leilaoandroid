package net.megamil.leilao24h.Utils.ToolBox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import net.megamil.leilao24h.Login.LoginActivity;
import net.megamil.leilao24h.Login.SigninActivity;
import net.megamil.leilao24h.Usuario.Busca.DB_Busca;
import net.megamil.leilao24h.Usuario.Fragmentos.Fragment_Leiloes;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Ativar_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Cadastra_Leilao;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;

import java.util.HashMap;
import java.util.Map;

import maripoppis.com.connection.Database.Database;
import maripoppis.com.socialnetwork.FaceBook.FacebookDataUtils;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;

/**
 * Created by John on 29/01/2018.
 */

public class ToolBox_Chama_Activity {

    /////
    /////  Tela a ser chama como principal do projeto
    /////
    public static void Funcao_Chama_TelaPrincipal(Activity activity) {
        Intent finalIntent = new Intent(activity, Pagina_Inicio.class);
        activity.startActivity(finalIntent);
        activity.finish();
    }

//    public static void Checa_Dados_Usuario(Activity activity, Context context) {
//
//        SharedPreferences prefs = context.getSharedPreferences("Tmp_ID", MODE_PRIVATE);
//        String usuario_ = prefs.getString("User", null);
//        String senha_ = prefs.getString("Senha", null);
//
//        if (usuario_ == null && senha_ == null) {
//            Chama_Tela_Login(activity, context);
//        }
//
//    }

    public static void Funcao_Logout(final Activity activity) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage("Deseja sair do sistema?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Funcao_RAW_Logout(activity);

                    }
                })

//                .setPositiveButton("Carregar Imagem", null)
                .setNegativeButton("Não", null)
                .create()
                .show();
    }



    public static void Funcao_excluir_imagem(final Activity activity) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage("Deseja sair do sistema?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Funcao_RAW_Logout(activity);

                    }
                })

//                .setPositiveButton("Carregar Imagem", null)
                .setNegativeButton("Não", null)
                .create()
                .show();
    }

    public static void Funcao_Excluir_Conta(final Activity activity) {

        final EditText input = new EditText(activity);

        new AlertDialog.Builder(activity)
                .setMessage("Deseja excluir sua conta?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new AlertDialog.Builder(activity)
                                .setMessage("Confirme sua senha:")
                                .setView(input)
                                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    //    Funcao_RAW_Logout(activity);

                                        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                                        String senhaSha1 = ToolBox_Criptografia.Funcao_Converter_String_To_SHA1(input.getText().toString());

                                        if(senhaSha1.equals(prefs.getTMP_HASH())){
                                            Funcao_Deletar_Usuario(activity);
                                        }else{
                                            Toast.makeText(activity, "Senha errada!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton("Cancelar", null)
                                .create()
                                .show();
                        //Funcao_RAW_Logout(activity);
                    }
                })

//                .setPositiveButton("Carregar Imagem", null)
                .setNegativeButton("Não", null)
                .create()
                .show();
    }


    public static void Funcao_Excluir_produto(final Activity activity,String id_produto) {

        final EditText input = new EditText(activity);

        new AlertDialog.Builder(activity)
                .setMessage("Deseja excluir o produto")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new AlertDialog.Builder(activity)
                                .setMessage("Confirme sua senha:")
                                .setView(input)
                                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //    Funcao_RAW_Logout(activity);

                                        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                                        String senhaSha1 = ToolBox_Criptografia.Funcao_Converter_String_To_SHA1(input.getText().toString());

                                        if(senhaSha1.equals(prefs.getTMP_HASH())){
                                            Funcao_Deletar_produto(activity,id_produto);
                                        }else{
                                            Toast.makeText(activity, "Senha errada!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setNegativeButton("Cancelar", null)
                                .create()
                                .show();
                        //Funcao_RAW_Logout(activity);
                    }
                })

//                .setPositiveButton("Carregar Imagem", null)
                .setNegativeButton("Não", null)
                .create()
                .show();
    }

    public static void Funcao_dialog_produto_deletar(final Activity activity,String mgs) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage(mgs)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { AtualizarActivity(activity);
                    }
                })
//                .setPositiveButton("Carregar Imagem", null)
                //.setNegativeButton("Não", null)
                .create()
                .show();
    }


    public static void Funcao_dialog_imagem_deletar(final Activity activity,Intent intent) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage("Imagem excluida com imagem")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { AtualizarActivity(activity,72,intent);
                    }
                })
//                .setPositiveButton("Carregar Imagem", null)
                //.setNegativeButton("Não", null)
                .create()
                .show();
    }





    public static void funcao_dialog_deletar_imagem(final Activity activity,String id_produto,int position,String imagem) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage("Deseja excluir está imagem?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        funcao_deletar_produto_imagem(activity,id_produto,position,imagem);
                    }
                })
//                .setPositiveButton("Carregar Imagem", null)
                .setNegativeButton("Não", null)
                .create()
                .show();
    }

    public static void Funcao_lance_editar(final Activity activity,String mgs) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage(mgs)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finalizarActivity(activity);
                    }
                })
//                .setPositiveButton("Carregar Imagem", null)
                //.setNegativeButton("Não", null)
                .create()
                .show();
    }


    public static void Funcao_Logout_deletar(final Activity activity,String mgs) {

        new android.app.AlertDialog.Builder(activity)
                .setMessage(mgs)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Funcao_RAW_Logout(activity);
                    }
                })
//                .setPositiveButton("Carregar Imagem", null)
                //.setNegativeButton("Não", null)
                .create()
                .show();
    }

    public static void Funcao_Deletar_Usuario(final Activity activity) {
        Dados_Ws(100, "", null, activity);
    }

    public static void Funcao_Deletar_Lance(final Activity activity, String id_leilao, String id_produto) {
        Map<String, String> Dados_Para_Parametros = new HashMap<>();
        Dados_Para_Parametros.put("id_leilao", id_leilao);
        Dados_Para_Parametros.put("fk_produto", id_produto);
        Dados_Ws(101, "", Dados_Para_Parametros, activity);
    }


    public static void Funcao_Deletar_produto(final Activity activity,String id_produto) {
        Map<String, String> Dados_Para_Parametros = new HashMap<>();
        Dados_Para_Parametros.put("id_produto", id_produto);
        Dados_Ws(101, "", Dados_Para_Parametros, activity);
    }

    public static void funcao_deletar_produto_imagem(final Activity activity,String id_produto,int posicao,String imagem) {
        Map<String, String> Dados_Para_Parametros = new HashMap<>();
        Dados_Para_Parametros.put("id_produto", id_produto);
        Dados_Para_Parametros.put("imagem[" + posicao + "]", imagem);
        Dados_Ws(72, "", Dados_Para_Parametros, activity);
    }


    public static void Funcao_RAW_Logout(final Activity activity) {

        final DB_Busca db_busca = new DB_Busca(activity);//conexao
        db_busca.DeletaBanco_Historico_Busca();

        ToolBox_SQlite db = new ToolBox_SQlite(activity);
        db.clear();

        ToolBox_SharedPrefs toolBox_sharedPrefs = new ToolBox_SharedPrefs(activity);
        toolBox_sharedPrefs.setTMP_ID("");
        toolBox_sharedPrefs.setTMP_HASH("");
        toolBox_sharedPrefs.setCancelDegug();

        ToolBox_SharedPrefs_Usuario toolBox_sharedPrefs_usuario = new ToolBox_SharedPrefs_Usuario(activity);
        toolBox_sharedPrefs_usuario.clear();

        FacebookDataUtils.FacebookLogout();

        Database database = new Database(activity);
        database.clear();

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
//                        activity.finishAffinity();

    }


    /*
    chama tela de localização de usuarios
    */

//    public static void Chama_Tela_Localiza_Cliente(Activity activity, Context context) {
//        Intent pagina_usuario = new Intent(context, Localiza_Cliente.class);
//        pagina_usuario.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(pagina_usuario);
//        activity.finish();
//    }

    /*
    chama tela de dados do cliente
    */
    public static void Chama_Tela(Activity activity, Intent intent) {
        activity.startActivity(intent);
        activity.finish();
    }


//    public static void Chama_Tela_Usuario_new(Activity activity, Context context) {
//        Intent intent = new Intent(context, Pagina_Usuario.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(intent);
//        activity.finish();
//    }

//    public static void Chama_Tela_Usuario(Context context, int indice) {
//        Intent intent = new Intent(context, Pagina_Usuario.class);
//        intent.putExtra("indice", indice);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(intent);
//    }


    //////////////////////////////////////////////////////////////////////////////////////
    /*
     *  Função para procurar e iniciar uma activity com ou sem parametros
     *
     *  0° View a ser clicada para chamar esta função
     *  1° Activity
     *  2° Classe a ser iniciada ***( caso nao existam putExtras , é so passar a classe a ser chamada , Do contrario , passar null )
     *  3° Intent ***( caso existam putExtras, pode criar a Intent e dar o start aqui, do contrario , mandar null )
     *  4° finaliza a tela atual ?
     *  5° mostra alerta para iniciar a tela??
     *  6° se mostrar alerta, qual o texto ?
     *
     *  exemplo de uso:
     *  ToolBox_Chama_Activity.Chama_Activity(activity, null, Carga_Bateria.class, true, false, "");
     */
    public static void Funcao_Chama_Activity(View view, final Activity activity, final Class classe_tela, final Intent intent, final Boolean finish, final Boolean alerta, final String texto_para_alerta) {

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent finalIntent;

                if (intent == null) {
                    finalIntent = new Intent(activity, classe_tela);
                } else {
                    finalIntent = intent;
                }

                finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (alerta) {
                    final Intent finalIntent1 = finalIntent;
                    new AlertDialog.Builder(activity)
                            .setMessage(texto_para_alerta)
                            .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    chamaactivity(activity, finalIntent1, finish);

                                }
                            })
                            .setNegativeButton("Não", null)
                            .create()
                            .show();
                } else {

                    chamaactivity(activity, finalIntent, finish);

                }


            }
        });


    }

    /////
    /////  Esta Função faz a mesma coisa que a Primeira , porem nao recebe um view
    /////  ou seja , so é chamada em eventos especificos detrminados pelo usuarios
    /////  não precisa ser especificamente um OnclickListenner!!!
    /////

    public static void Funcao_Chama_Activity(final Activity activity, final Class classe_tela, final Intent intent, final Boolean finish, final Boolean alerta, final String texto_para_alerta) {

        Intent finalIntent;

        if (intent == null) {
            finalIntent = new Intent(activity, classe_tela);
        } else {
            finalIntent = intent;
        }

        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (alerta) {
            final Intent finalIntent1 = finalIntent;
            new AlertDialog.Builder(activity)
                    .setMessage(texto_para_alerta)
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            chamaactivity(activity, finalIntent1, finish);
                        }
                    })
                    .setNegativeButton("Não", null)
                    .create()
                    .show();
        } else {
            chamaactivity(activity, finalIntent, finish);
        }
    }




    public static void Funcao_Chama_Activity_For_Result(final Activity activity, final Class classe_tela, final Intent intent,int requestCode, final Boolean finish, final Boolean alerta, final String texto_para_alerta) {

        Intent finalIntent;

        if (intent == null) {
            finalIntent = new Intent(activity, classe_tela);
        } else {
            finalIntent = intent;
        }

        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (alerta) {
            final Intent finalIntent1 = finalIntent;
            new AlertDialog.Builder(activity)
                    .setMessage(texto_para_alerta)
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            chamaActivityForResult(activity, finalIntent1,requestCode, finish);
                        }
                    })
                    .setNegativeButton("Não", null)
                    .create()
                    .show();
        } else {
            chamaactivity(activity, finalIntent, finish);
        }
    }
    /////
    /////  complemento para a Primeira e Segunda funções a cima
    /////
    private static void chamaactivity(Activity activity, Intent intent, Boolean finish) {
        activity.startActivity(intent);
        if (finish) {
            activity.finish();
        }
    }

    private static void chamaActivityForResult(Activity activity, Intent intent,int requestCode, Boolean finish) {
        activity.startActivityForResult(intent, requestCode,null);
        if (finish) {
            activity.finish();
        }
    }

    private static void finalizarActivity(Activity activity) {
        Intent intent = new Intent();
        intent.putExtra("excluir",true);
        Fragment_Leiloes fragment_leiloes = new Fragment_Leiloes();
        fragment_leiloes.onActivityResult( 101,  Activity.RESULT_OK,  intent);
        activity.finish();
    }


    private static void AtualizarActivity(Activity activity) {
        Intent intent = new Intent();
        Ativar_Leilao fragment_leiloes = ((Ativar_Leilao)activity);
        fragment_leiloes.onActivityResult( 101,  Activity.RESULT_OK,  intent);
    }


    private static void AtualizarActivity(Activity activity, int requestCode, Intent intent) {

        Cadastra_Leilao cadastra_Leilao = ((Cadastra_Leilao)activity);
        cadastra_Leilao.onActivityResult( requestCode,  Activity.RESULT_OK,  intent);
    }
}
