package net.megamil.leilao24h.Utils.Conexao_Volley;

import net.megamil.leilao24h.BuildConfig;

/**
 * Created by nalmir on 30/11/2017.
 */

public class Conexao_Constantes {

    public static String Base_URL_WS = BuildConfig.CONECTION_BASE_URL;
    private static String PROJECT_NAME = BuildConfig.CONECTION_PROJECT_NAME;
    private static String URL_WS = Base_URL_WS + PROJECT_NAME;

    public static final String SOCKET_WS = Base_URL_WS + BuildConfig.CONECTION_SOCKET_PORT;
    public static final String URL_WS_PADRAO = URL_WS + BuildConfig.CONECTION_CONTROLLER_WEBSERVICE;
    public static final String URL_WS_PACOTES = URL_WS + "comprar_ingressos/";
    public static final String URL_WS_FOTO_PERFIL = URL_WS + "upload/perfil/foto/";

}
