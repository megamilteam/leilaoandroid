package net.megamil.leilao24h.Utils.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by nalmir on 24/11/2017.
 */

public class Dao {

    private Context context;
    protected SQLiteDatabase db;

    public Dao(Context context) {
        this.context = context;
    }

    public void abrirBanco(){
        SQLiteHelper helper = new SQLiteHelper(
                context,
                Constantes_Banco.BANCO,
                null,
                Constantes_Banco.VERSAO
        );

        this.db = helper.getWritableDatabase();

    }

    public void fecharBanco(){
        if (db !=  null){
            db.close();
        }
    }
}
