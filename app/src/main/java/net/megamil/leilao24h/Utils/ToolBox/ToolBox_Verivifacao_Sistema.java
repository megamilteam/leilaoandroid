package net.megamil.leilao24h.Utils.ToolBox;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public abstract class ToolBox_Verivifacao_Sistema {

    /**
     *
     * @param context
     * @return
     */

    public static boolean Funcao_Status_Conexao(Context context) {
        boolean connect;
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            connect = true;
        } else {
            connect = false;
        }
        return connect;
    }

}
