package net.megamil.leilao24h.Utils.ToolBox;

import android.app.Activity;
import android.content.ContentResolver;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;

/**
 * Created by John on 09/12/2017.
 */

public class ToolBox_RingTone {

    public static void Funcao_Play(Activity activity){

        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + activity.getPackageName() + "/raw/judgegavel");

//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        Ringtone ringtone = RingtoneManager.getRingtone(activity,uri);
        ringtone.play();

    }

    public static void Funcao_RT_Uri(Activity activity){

        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + activity.getPackageName() + "/raw/zelda_treasure");

//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        Ringtone ringtone = RingtoneManager.getRingtone(activity,uri);
        ringtone.play();

    }

    public static void Funcao_RT_MediaPlayer(Activity activity){

        MediaPlayer player = MediaPlayer.create(activity, Settings.System.DEFAULT_RINGTONE_URI);
        player.start();

    }

}
