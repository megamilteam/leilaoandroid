package net.megamil.leilao24h.Utils.model;

/**
 * Created by nalmir on 12/12/2017.
 */

public class Model_Busca {

    private long id;
    private String texto;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
