package net.megamil.leilao24h.Utils.ToolBox;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

import net.megamil.leilao24h.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by John on 09/12/2017.
 */

public class ToolBox_Android_Intent {

    public void intentOpenDocument(Activity activity, int CODE_REQUEST_RESULT) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            activity.startActivityForResult(new Intent(Intent.ACTION_GET_CONTENT).setType("image/*"),
                    CODE_REQUEST_RESULT);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            activity.startActivityForResult(intent, CODE_REQUEST_RESULT);
        }
    }

    /**
     * Create and start intent to share a standard text value.
     *
     * @param activity
     * @param textTitle Title Dilaog
     * @param textShare text to Share
     */
    public static void intentShareText(Activity activity, String textTitle, String textShare) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, textShare);
        activity.startActivity(Intent.createChooser(shareIntent, textTitle));
    }

    /**
     * Create and start intent to share a photo with apps that can accept a single image
     * of any format.
     * <p>
     * Exemplo:
     * String textTitle = "";
     * String textShare = "";
     * String appName = "";
     * String linkApp = "play.google.com/store/apps/details?id=com.sega.comixzone";
     * intentShareTextWithApliccation(activity, textTitle, textShare, appName, linkApp);
     *
     * @param activity
     * @param textTitle
     * @param textShare
     * @param appName
     * @param linkApp
     */

    public static void intentShareTextWithApliccation(Activity activity, String textTitle, String textShare, String appName, String linkApp) {
        try {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, appName);
            String sAux = linkApp + " " + textShare;
            shareIntent.putExtra(Intent.EXTRA_TEXT, sAux);
            activity.startActivity(Intent.createChooser(shareIntent, textTitle));
        } catch (Exception e) {
            //e.toString();
        }

    }


    public static void intentShareTextWithThisApliccation(Activity activity, String textTitle, String textShare) {
        https:
        try {
            String appName = activity.getResources().getString(R.string.app_name);
            String linkApp = "http://play.google.com/store/apps/details?id=" + activity.getPackageName();

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, appName);
            String sAux = linkApp + " " + textShare;
            shareIntent.putExtra(Intent.EXTRA_TEXT, sAux);
            activity.startActivity(Intent.createChooser(shareIntent, textTitle));
        } catch (Exception e) {
            //e.toString();
        }

    }

    public static void intentShareTextHTML(Activity activity, String textTitle, String textShare, String appName, String linkApp) {
        https:
        try {

            String textToShare = textShare+"\nVisit <a href=" + linkApp + ">"+appName+"</a> for more info.";
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);

            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_SUBJECT, appName);
            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(textToShare));

            activity.startActivity(Intent.createChooser(intent, textTitle));
        } catch (Exception e) {
            //e.toString();
        }

    }


    /**
     * Create and start intent to share a photo with apps that can accept a single image
     * of any format.
     */
//    public static void intentShareTextWithOneImages(Activity activity, String textTitle , String textShare) {
//        Uri path = FileProvider.getUriForFile(activity, "com.restart.sharingdata", new File(ic_android_green));
//        Intent shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, "This is one image I'm sharing.");
//        shareIntent.putExtra(Intent.EXTRA_STREAM, path);
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        shareIntent.setType("image/*");
//        activity.startActivity(Intent.createChooser(shareIntent, "Share..."));
//    }

    /**
     * Create and start intent to share multiple photos with apps that can accept images
     * of any format.
     */
    public static void intentShareTextWithMultiImages(Activity activity, String
            textTitle, String textShare) {
//        ArrayList<Uri> photos = new ArrayList<>();
//        photos.add(FileProvider.getUriForFile(activity, "com.restart.sharingdata", new File(ic_android_white)));
//        photos.add(FileProvider.getUriForFile(activity, "com.restart.sharingdata", new File(ic_android_green)));
//        photos.add(FileProvider.getUriForFile(activity, "com.restart.sharingdata", new File(ic_android_red)));
//
//        Intent shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, "This multiple images I'm sharing.");
//        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, photos);
//        shareIntent.setType("image/*");
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        activity.startActivity(Intent.createChooser(shareIntent, "Share..."));
    }


//            mbtn_googlemap.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
////                String uri = "geo:0,0?q="+latLngPartida.latitude+","+latLngPartida.longitude;
//            String uri = "http://maps.google.com/maps?f=d&hl=en&saddr=" + latLngPartida.latitude + "," + latLngPartida.longitude + "&daddr=" + latLngDestino.latitude + "," + latLngDestino.longitude;
//            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//            startActivity(Intent.createChooser(intent, "Selecione a Baixo"));
//            Novas_Funcoes.Funcao_Mostra_Layout(rl_pai, ll_corridaaceita);
//            Novas_Funcoes.Funcao_Ocultar_Layout(MainActivity.this, ll_mapas);
//        }
//    });
//
//        mbtn_atecliente.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            String uri = "geo:0,0?q=" + latLngPartida.latitude + "," + latLngPartida.longitude;
//////                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="+latLngPartida.latitude+","+latLngPartida.longitude+"&daddr="+latLngDestino.latitude+","+latLngDestino.longitude;
//            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//            startActivity(Intent.createChooser(intent, "Selecione um App para ajudar a chegar ao cliente"));
//            Novas_Funcoes.Funcao_Mostra_Layout(rl_pai, ll_corridaaceita);
//            Novas_Funcoes.Funcao_Ocultar_Layout(MainActivity.this, ll_mapas);
//        }
//    });
//
//        mbtn_atedestino.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            String uri = "geo:0,0?q=" + latLngDestino.latitude + "," + latLngDestino.longitude;
//////                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="+latLngPartida.latitude+","+latLngPartida.longitude+"&daddr="+latLngDestino.latitude+","+latLngDestino.longitude;
//            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//            startActivity(Intent.createChooser(intent, "Selecione um app para verificar o destino!"));
//            Novas_Funcoes.Funcao_Mostra_Layout(rl_pai, ll_corridaaceita);
//            Novas_Funcoes.Funcao_Ocultar_Layout(MainActivity.this, ll_mapas);
//        }
//    });

    /**
     * Share on FacebookData. Using FacebookData app if installed or web link otherwise.
     *
     * @param activity activity which launches the intent
     * @param text     not used/allowed on FacebookData
     * @param url      url to share
     */
    public static void shareFacebook(Activity activity, String text, String url) {
        boolean facebookAppFound = false;
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(url));

        PackageManager pm = activity.getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        for (final ResolveInfo app : activityList) {
            if ((app.activityInfo.packageName).contains("com.facebook.katana")) {
                final ActivityInfo activityInfo = app.activityInfo;
                final ComponentName name = new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setComponent(name);
                facebookAppFound = true;
                break;
            }
        }
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + url;
            shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }
        activity.startActivity(shareIntent);
    }

    /**
     * Share on Twitter. Using Twitter app if installed or web link otherwise.
     *
     * @param activity activity which launches the intent
     * @param text     text to share
     * @param url      url to share
     * @param via      twitter username without '@' who shares
     * @param hashtags hashtags for tweet without '#' and separated by ','
     */
    public static void shareTwitter(Activity activity, String text, String url, String via, String hashtags) {
        StringBuilder tweetUrl = new StringBuilder("https://twitter.com/intent/tweet?text=");
        tweetUrl.append(TextUtils.isEmpty(text) ? urlEncode(" ") : urlEncode(text));
        if (!TextUtils.isEmpty(url)) {
            tweetUrl.append("&url=");
            tweetUrl.append(urlEncode(url));
        }
        if (!TextUtils.isEmpty(via)) {
            tweetUrl.append("&via=");
            tweetUrl.append(urlEncode(via));
        }
        if (!TextUtils.isEmpty(hashtags)) {
            tweetUrl.append("&hastags=");
            tweetUrl.append(urlEncode(hashtags));
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl.toString()));
        List<ResolveInfo> matches = activity.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                intent.setPackage(info.activityInfo.packageName);
            }
        }
        activity.startActivity(intent);
    }

    /**
     * Share on Whatsapp (if installed)
     *
     * @param activity activity which launches the intent
     * @param text     text to share
     * @param url      url to share
     */
    public static void shareWhatsapp(Activity activity, String text, String url) {
        PackageManager pm = activity.getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");

            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");

            waIntent.putExtra(Intent.EXTRA_TEXT, text + " " + url);
//            activity.startActivity(Intent
//                    .createChooser(waIntent, activity.getString(R.string.share_intent_title)));

        } catch (PackageManager.NameNotFoundException e) {
//            Toast.makeText(activity, activity.getString(R.string.share_whatsapp_not_instaled),
//                    Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Convert to UTF-8 text to put it on url format
     *
     * @param s text to be converted
     * @return text on UTF-8 format
     */
    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("wtf", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }
}
