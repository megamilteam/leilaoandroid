package net.megamil.leilao24h.Utils.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nalmir on 23/11/2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            StringBuilder sb = new StringBuilder();
          sb.append("CREATE TABLE IF NOT EXISTS [logado] (\n" +
                  "  [id] TEXT, \n" +
                  "  CONSTRAINT [] PRIMARY KEY ([id]));");

            String[] comandos = sb.toString().split(";");

            for (int i = 0; i < comandos.length; i++) {
                db.execSQL(comandos[i].toLowerCase());
            }
        } catch (Exception e){
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            StringBuilder sb = new StringBuilder();
            //
            sb.append("DROP TABLE IF EXISTS  [contatos];");

            //Segunda Tabela
            sb.append("DROP TABLE IF EXISTS  [contatos_n];");

            String[] comandos = sb.toString().split(";");

            for (int i = 0; i < comandos.length; i++) {
                db.execSQL(comandos[i].toLowerCase());
            }
        } catch (Exception e){
        }

        onCreate(db);
    }
}
