/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/2/3 at 1:9:18 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Project.DialogPayment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import net.megamil.leilao24h.Login.LoginActivity;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.library.Utils.ActivityUtil;
import net.megamil.library.Utils.DialogUtils;

import java.util.List;

import maripoppis.com.connection.Constants.ConnectionUrl;
import maripoppis.com.connection.Model.SubModels.Pendencias;
import maripoppis.com.connection.Model.WSResponse;

public class DialogPayment {

    public static void openDialogPayment(final Activity activity, final WSResponse response) {

        View view = DialogUtils.getView(activity, R.layout.dialog_list_payment);
        final Dialog dialog = DialogUtils.openDialogNoBackground(activity, view);

        List<Pendencias> database = response.getPendencias();
        int lenght = database.size();
//        int lenght = 1;
        final RecyclerView recyclerView = view.findViewById(R.id.recyclerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        AdapterPayment payment = new AdapterPayment(activity, database);
        recyclerView.setAdapter(payment);
        payment.setOnItemClickListener(new AdapterPayment.OnClickItemListener() {
            @Override
            public void itenView(Pendencias pendencias, int position) {
                ActivityUtil.callActivityWebView(activity, ConnectionUrl.getUrlPayment(response.getDados().getIdUsuario(), pendencias.getIdLeilao()), false);
                dialog.dismiss();
            }
        });


        if (lenght <= 1) {
            Button button = view.findViewById(R.id.btnGetCredits);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LoginActivity.goLogin(response);
                    dialog.dismiss();
                }
            });
            button.setVisibility(View.VISIBLE);
        }
    }

    private static int numShowDialog = 1;

    public static void openDialogPayment(final Activity activity, final List<Pendencias> dataList, final String idUsuario) {
        int lenght = dataList.size();
//        int lenght = 2;
        if (lenght > 0) {
            if (numShowDialog == 2 || lenght > 1) {

                View view = DialogUtils.getView(activity, R.layout.dialog_list_payment);
                final Dialog dialog = DialogUtils.openDialogNoBackground(activity, view);


//        int lenght = 1;
                final RecyclerView recyclerView = view.findViewById(R.id.recyclerList);
                recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
                AdapterPayment payment = new AdapterPayment(activity, dataList);
                recyclerView.setAdapter(payment);
                payment.setOnItemClickListener(new AdapterPayment.OnClickItemListener() {
                    @Override
                    public void itenView(Pendencias pendencias, int position) {
                        ActivityUtil.callActivityWebView(activity, ConnectionUrl.getUrlPayment(idUsuario, pendencias.getIdLeilao()), false);
                        dialog.dismiss();
                    }
                });


                if (lenght <= 1) {
                    Button button = view.findViewById(R.id.btnGetCredits);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    button.setVisibility(View.VISIBLE);
                } else {
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            ToolBox_Chama_Activity.Funcao_RAW_Logout(activity);
                        }
                    });
                }
                numShowDialog = 0;
            } else {
                numShowDialog++;
            }
        }
    }
}
