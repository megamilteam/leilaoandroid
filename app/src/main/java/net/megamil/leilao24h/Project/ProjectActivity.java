/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Project;

import android.app.Activity;
import android.content.Intent;

import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Planos;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.PublicSale;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Cadastra_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Editar_Usuario;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao.Activity_Ativar_Leilao;
import net.megamil.leilao24h.Utils.Api_Login.FireBase.LeilaoFirebaseMessagingService;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.library.Utils.ActivityUtil;

import org.json.JSONException;
import org.json.JSONObject;

import maripoppis.com.connection.Model.SubModels.Leiloes;
import maripoppis.com.connection.Model.SubModels.Produtos;

@SuppressWarnings("all")
public class ProjectActivity {

    //==============================================================================================
    //
    // ** Keys for Activitys
    //
    //==============================================================================================

    public static String KEY_PUBLICSALE = "public_sale_data";

    //==============================================================================================
    //
    // ** Activitys
    //
    //==============================================================================================

    public static void goCreatePublicSaleActivity(Activity activity) {
        ActivityUtil.callActivity(activity, new Intent(activity, Cadastra_Leilao.class), false);
    }

    public static void goEditPublicSaleActivity(Activity activity, Produtos produtos) {
        Intent intent = new Intent(activity, Cadastra_Leilao.class);
        intent.putExtra("database", produtos);
        ActivityUtil.callActivity(activity, intent, false);
    }

    public static void goDeletePublicSaleActivity(Activity activity, Produtos produtos) {
        ToolBox_Chama_Activity.Funcao_Excluir_produto(activity,produtos.getIdProduto());
    }

    public static void goPublicSaleActivity(Activity activity, Leiloes leiloes) {
        Intent intent = new Intent(activity, PublicSale.class);
        intent.putExtra(KEY_PUBLICSALE, leiloes);
        ActivityUtil.callActivity(activity, intent, false);
    }

    public static void goPublicSaleActivityByNotification(Activity activity, String idLeilao) {
        Intent intent = new Intent(activity, PublicSale.class);
        intent.putExtra(LeilaoFirebaseMessagingService.FB_NORTIFICATION_ID, idLeilao);
        ActivityUtil.callActivity(activity, intent, true);
    }

    public static void goCreditsPurchaseWithMsg(Activity activity) {
        ToolBox_Chama_Activity.Funcao_Chama_Activity(
                activity,
                Planos.class,
                null,
                false,
                true,
                "Deseja comprar mais ingressos?");
    }

    public static void goCreditsPurchase(Activity activity) {
        Intent intent = new Intent(activity, Planos.class);
        ActivityUtil.callActivity(activity, intent, false);
    }

    public static void goPerfilActivity(Activity activity, JSONObject objeto) {
        Intent intent = new Intent(activity, Editar_Usuario.class);
        try {
            JSONObject usuario = objeto.getJSONObject("usuarios");
            intent.putExtra("comp", usuario.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ActivityUtil.callActivity(activity, intent, false);
    }

    public static void goCreatePublicSale(final Activity activity, final HMAux hmAux, final int position) {
        Intent intent = new Intent(activity, Activity_Ativar_Leilao.class);
        intent.putExtra(HMAux.ID_POSITION, position);
        intent.putExtra(HMAux.INTENT_TYPE, 2);
        intent.putExtra(HMAux.ID_PROCUTO, hmAux.get(HMAux.ID_PROCUTO));
        intent.putExtra(HMAux.NOME_PRODUTO, hmAux.get(HMAux.NOME_PRODUTO));
        intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux.get(HMAux.DESCRICAO_PRODUTO));
        intent.putExtra(HMAux.NOME_CATEGORIA, hmAux.get(HMAux.NOME_CATEGORIA));
        intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux.get(HMAux.IMAGENS_ARRAY));
        ActivityUtil.callActivity(activity, intent, false);
    }


}
