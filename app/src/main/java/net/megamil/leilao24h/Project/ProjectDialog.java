/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Project;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.PublicSale;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao.Activity_Ativar_Leilao;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Recompensa;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.library.Utils.ActivityUtil;
import net.megamil.library.Utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

import maripoppis.com.connection.Constants.ConnectionContants;
import maripoppis.com.connection.Constants.ConnectionUrl;
import maripoppis.com.connection.Model.SubModels.Leiloes;
import maripoppis.com.connection.Model.SubModels.Produtos;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.GlideUtil.initGlide;

public class ProjectDialog {

    public static void showDetails(final Activity activity, final HMAux magazine, final int position) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_magazine);

        //==========================================================================================

        LinearLayout labelNew = dialog.findViewById(R.id.labelNew);
        if (magazine.get(HMAux.PRODUTO_USADO).equals("0")) {
            labelNew.setVisibility(View.VISIBLE);
        } else {
            labelNew.setVisibility(View.GONE);
        }
        //==========================================================================================

        LinearLayout labelCategory = dialog.findViewById(R.id.labelCategory);
        if (!magazine.get(HMAux.NOME_CATEGORIA).equals("")) {
            labelCategory.setVisibility(View.VISIBLE);
            ((TextView) dialog.findViewById(R.id.numberPrice)).setText(magazine.get(HMAux.NOME_CATEGORIA));
        } else {
            labelCategory.setVisibility(View.GONE);
            ((TextView) dialog.findViewById(R.id.numberPrice)).setText("");
        }

        //==========================================================================================

        initGlide(activity, Conexao_Constantes.Base_URL_WS + magazine.get(HMAux.IMAGENS), ((ImageView) dialog.findViewById(R.id.magazineCover)));

        //==========================================================================================

        ((TextView) dialog.findViewById(R.id.magazineName)).setText(magazine.get(HMAux.NOME_PRODUTO));
        ((TextView) dialog.findViewById(R.id.description)).setText(magazine.get(HMAux.DESCRICAO_PRODUTO));
        ((TextView) dialog.findViewById(R.id.ingressos)).setText(magazine.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));

        //==========================================================================================

        dialog.findViewById(R.id.btnMoreDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                int value = Integer.parseInt(magazine.get(HMAux.PRODUTO_RECOMPENSA_CUSTO));
                if (Recompensa.points < value) {
                    MsgUtil.Funcao_MSG(activity, "Pontos insuficientes");
                } else {
                    new AlertDialog.Builder(activity)
                            .setMessage("Adquirir?")
                            .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Map<String, String> map = new ArrayMap<>();
                                    map.put("tipo_recompensa", "2");
                                    map.put("id_recompensa", magazine.get(HMAux.ID_PRODUTO));
                                    Dados_Ws(24, "", map, activity);
                                }
                            })
                            .setNegativeButton("Não", null)
                            .create()
                            .show();
                }
            }
        });

        //==========================================================================================

        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();
    }

    public static void showDetails(final Activity activity, final Leiloes leiloes, final int position) {

        View view = DialogUtils.getView(activity, R.layout.dialog_info_publicsale);

        //Image Public Sale
        //==========================================================================================

        ToolBox_Imagem.initGlide(
                activity,
                ConnectionUrl.MakeUrlImage(leiloes.getImagens().get(0)),
                (ImageView) view.findViewById(R.id.img_item)
        );

        // ** Texts
        //==============================================================================================

        ((TextView) view.findViewById(R.id.tvLabelNew)).setText(ProjectUtils.verifyNewOrUsed(leiloes.getProdutoUsado()));
        ((TextView) view.findViewById(R.id.name_item)).setText(leiloes.getNomeProduto());
        ((TextView) view.findViewById(R.id.desc_item)).setText(leiloes.getDescricaoProduto());
        ((TextView) view.findViewById(R.id.ingressos)).setText(leiloes.getIngressos());

        ProjectUtils.verifyInitPublicSale(activity,
                ((TextView) view.findViewById(R.id.tv_title_data_inicio)),
                ((TextView) view.findViewById(R.id.data_inicio)),
                leiloes.getDataInicio(),
                leiloes.getDataFimPrevisto());

        // ** Init Layout and Show Dilog
        //==============================================================================================
        DialogUtils.openDialog(activity, view);

    }

    //==============================================================================================
    //
    // ** More Credits
    //
    //==============================================================================================

    public static void openMoreCredits(final Activity activity) {

        View view = qunaticheart.com.animations.DialogUtils.getView(activity, R.layout.dialog_more_credit);
        final Dialog dialog = DialogUtils.openDialogNoBackground(activity, view);

        view.findViewById(R.id.btnGetCredits).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectActivity.goCreditsPurchase(activity);
                dialog.dismiss();
            }
        });
    }

    //==============================================================================================
    //
    // ** Select Create Public sale or Edit Public sale
    //
    //==============================================================================================

    public static void openSelectMenuEditOrCreate(final Activity activity, final HMAux hmAux, final int position) {

        View view = qunaticheart.com.animations.DialogUtils.getView(activity, R.layout.dialog_menu_public_sale);
        final Dialog dialog = DialogUtils.openDialogNoBackground(activity, view);

        view.findViewById(R.id.tvCreatePublicSale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectActivity.goCreatePublicSale(activity, hmAux, position);
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.tvEditPublicSale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("TESTE", hmAux.toString());
                JSONObject object = new JSONObject(hmAux);
                Log.w("TESTE", object.toString());
                Gson gson = new Gson();
                Produtos produtos = gson.fromJson(object.toString(), Produtos.class);

                ProjectActivity.goEditPublicSaleActivity(activity, produtos);
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.tvDeletePublicSale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("TESTE", hmAux.toString());
                JSONObject object = new JSONObject(hmAux);
                Log.w("TESTE", object.toString());
                Gson gson = new Gson();
                Produtos produtos = gson.fromJson(object.toString(), Produtos.class);

                ProjectActivity.goDeletePublicSaleActivity(activity, produtos);
                dialog.dismiss();
            }
        });

    }
}
