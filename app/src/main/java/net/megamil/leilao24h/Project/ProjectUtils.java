/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Project;

import android.app.Activity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;

import java.util.ArrayList;
import java.util.List;

import maripoppis.com.connection.Constants.ConnectionUrl;

import static net.megamil.leilao24h.Utils.ToolBox.MsgUtil.debugLog;

public class ProjectUtils {

    public static String verifyNewOrUsed(String s) {
        if (Integer.parseInt(s) == 0) {
            return "Novo";
        } else {
            return "Usado";
        }
    }

    public static void verifyInitPublicSale(Activity activity,
                                            TextView labelInitPublicSale,
                                            TextView labelDataPublicSale,
                                            String dataStart,
                                            String dataFinish) {

        if (!ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Agora(dataFinish)) {
            labelInitPublicSale.setText("Finalizado");
            labelInitPublicSale.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            labelDataPublicSale.setVisibility(View.INVISIBLE);
        } else {
            if (ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Agora(dataStart)) {
                labelInitPublicSale.setText("Inicia em:");
            } else {
                labelInitPublicSale.setText("Iniciou em:");
                labelInitPublicSale.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            }
        }

        labelDataPublicSale.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(dataStart, false, false));
    }

    public static void verifyFinishPublicSale(Activity activity,
                                            TextView labelInitPublicSale,
                                            TextView labelDataPublicSale,
                                            String dataStart,
                                            String dataFinish) {

        if (!ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Agora(dataFinish)) {
            labelInitPublicSale.setText("Finalizado");
            labelInitPublicSale.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            labelDataPublicSale.setVisibility(View.INVISIBLE);
        } else {
            if (ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Agora(dataStart)) {
                labelInitPublicSale.setText("Inicia em:");
                labelDataPublicSale.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(dataStart, false, false));

            } else {
                labelInitPublicSale.setText("Finaliza em:");
                labelInitPublicSale.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                labelDataPublicSale.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(dataFinish, false, false));

            }
        }
    }

    public static void verifyFavoriteStatus(ImageView imageFavorite, String favoriteStatus) {
        AlphaAnimation alphaAnimationShowIcon = new AlphaAnimation(0.2f, 1.0f);
        alphaAnimationShowIcon.setDuration(500);

        if (favoriteStatus.equals("0")) {
            imageFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            imageFavorite.startAnimation(alphaAnimationShowIcon);
            debugLog("Not Favorite");
        } else {
            imageFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
            imageFavorite.startAnimation(alphaAnimationShowIcon);
            debugLog("Favorite");
        }
    }

    public static List<String> getFinalListUrlImage(List<String> listBaseUrl) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < listBaseUrl.size(); i++) {
            list.add(ConnectionUrl.MakeUrlImage(listBaseUrl.get(i)));
        }
        return list;
    }
}
