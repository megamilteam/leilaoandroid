/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/2/3 at 1:9:4 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Project.DialogPayment;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import net.megamil.leilao24h.R;

import java.util.List;

import maripoppis.com.connection.Model.SubModels.Pendencias;

public class AdapterPayment extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //First
    private Activity activity;
    private String idUsuario;
    private List<Pendencias> dataBase;
    private LayoutInflater layoutInflater;

    AdapterPayment(Activity activity, List<Pendencias> dataBase) {
        this.activity = activity;
        this.dataBase = dataBase;
        this.layoutInflater = LayoutInflater.from(activity);
    }

    /**
     * @return HMAux's List size
     */
    @Override
    public int getItemCount() {
        return dataBase.size();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator animatorUP = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    animatorUP.setDuration(200);
                    animatorUP.setInterpolator(new DecelerateInterpolator());
                    animatorUP.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator animatorDOWN = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    animatorDOWN.setDuration(200);
                    animatorDOWN.setInterpolator(new AccelerateInterpolator());
                    animatorDOWN.start();
                    break;
            }
            return false;
        }
    };

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(layoutInflater.inflate(R.layout.cell_payment, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Pendencias pendencias = dataBase.get(position);
        if (holder instanceof RecyclerViewHolder) {
            RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
            viewHolder.view.setOnTouchListener(touchListener);
            viewHolder.view.setTag(position);
            viewHolder.bind(pendencias);
            viewHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null)
                        mListener.itenView(pendencias, position);
                }
            });
        }
    }

    /**
     * ViewHolder NORMAL_TYPE
     */
    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private TextView name_item;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            name_item = itemView.findViewById(R.id.text);
        }

        private void bind(Pendencias pendencias) {
            name_item.setText(pendencias.getNomeProduto());
        }
    }

    //==============================================================================================
    //
    // ** Interface
    //
    //==============================================================================================

    private OnClickItemListener mListener;
    public void setOnItemClickListener(OnClickItemListener listener) {
        mListener = listener;
    }
    public interface OnClickItemListener {
        void itenView(Pendencias pendencias, int position);
    }

}
