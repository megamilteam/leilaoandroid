/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\  /===/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/2/9 at 7:42:10 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Login;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;

import net.megamil.leilao24h.Usuario.DebugActivity;

import maripoppis.com.connection.Connection;
import maripoppis.com.connection.Model.WSResponse;

public abstract class BaseConectionActivity extends DebugActivity implements Connection.ConnectCallback {

    //conection
    public Connection connection;
    public Activity activity;

    //==============================================================================================
    //
    // ** OnCreate
    //
    //==============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = BaseConectionActivity.this;
    }

    //==============================================================================================
    //
    // ** Conection Callback
    //
    //==============================================================================================

    @Override
    public void ConnectionSuccess(WSResponse response, Connection.ConnectionType connectionTypeID) {
        ConnectionSuccessfulCallback(response, connectionTypeID);
    }

    @Override
    public void ConnectionError(int statusType, WSResponse response, Connection.ConnectionType connectionTypeID) {
        ConnectionErrorCallback(statusType, response, connectionTypeID);
    }

    @Override
    public void ConnectionFail(Throwable t) {
        ConnectionFailCallback(t);
    }

    //==============================================================================================
    //
    // ** Abstract callback
    //
    //==============================================================================================

    public abstract void ConnectionSuccessfulCallback(WSResponse response, Connection.ConnectionType connectionTypeID);

    public abstract void ConnectionErrorCallback(int statusType, WSResponse response, Connection.ConnectionType connectionTypeID);

    public abstract void ConnectionFailCallback(Throwable t);

    //==============================================================================================
    //
    // ** Create Connection Service
    //
    //==============================================================================================

    public void startConectService() {
        Connection.setCallback(this);
        connection = new Connection(activity);
    }

    //==============================================================================================
    //
    // ** @Override
    //
    //==============================================================================================

    @Override
    protected void onResume() {
        super.onResume();
        startConectService();
    }
}
