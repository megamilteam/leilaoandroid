package net.megamil.leilao24h.Login;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.DebugActivity;


public class Politica_Privacidade extends DebugActivity {

    private Context context;
    private Window window;

    ImageButton imgButtonVoltar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica_privacidade);

        initVars();
//        initActions();
    }

    private void initVars() {
        imgButtonVoltar = findViewById(R.id.btn_politica_privacidade_voltar_login);
        context = getApplicationContext();
        window = getWindow();

        WebView view = new WebView(this);
        view.setVerticalScrollBarEnabled(false);

        ((LinearLayout)findViewById(R.id.relativeLayout2)).addView(view);
//        view.loadData(getString(R.string.tela_politica_de_privacidade), "text/html; charset=utf-8", "utf-8");
    }

//    private void initActions() {
//        imgButtonVoltar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ChamaPrimeiraTela();
//            }
//        });
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        ChamaPrimeiraTela();
//    }

//    private void ChamaPrimeiraTela() {
//        Intent intent = new Intent(context, Pagina_Login.class);
//        startActivity(intent);
//        finish();
//    }


}


