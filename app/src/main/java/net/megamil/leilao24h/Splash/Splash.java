/*
 * Copyright (c) Developed by John Alves at 2018/11/3.
 */

package net.megamil.leilao24h.Splash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import net.megamil.leilao24h.Login.LoginActivity;
import net.megamil.leilao24h.Project.ProjectActivity;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;
import net.megamil.leilao24h.Utils.Api_Login.FireBase.LeilaoFirebaseMessagingService;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox._ToolBox_Config_Inicial;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;

public class Splash extends AppCompatActivity {

    private static Context context;

    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._00_splash);
        context = getApplicationContext();
        bundle = getIntent().getExtras();

        _ToolBox_Config_Inicial.Funcao_Verifica_Pastas();
        Verifica_Usuario_Esta_Logado();//Pega do prefs se já está criado

    }


    public static void verifyIsLogued(Activity activity) {
        ToolBox_SharedPrefs_Usuario pres = new ToolBox_SharedPrefs_Usuario(activity);
        Boolean splash = pres.getLOGADO();

        if (!splash) {
            final Intent Pagina_Inicio = new Intent(activity, LoginActivity.class);
            activity.startActivity(Pagina_Inicio);
            activity.finish();
        }

    }

    private void Verifica_Usuario_Esta_Logado() {

        ToolBox_SharedPrefs_Usuario pres = new ToolBox_SharedPrefs_Usuario(context);
        Boolean splash = pres.getLOGADO();

        if (splash) {

            if (bundle.size() > 0 && bundle.containsKey(LeilaoFirebaseMessagingService.FB_NORTIFICATION_ID)) {

                ProjectActivity.goPublicSaleActivityByNotification(this, bundle.getString(LeilaoFirebaseMessagingService.FB_NORTIFICATION_ID));

            } else {

                final Intent Pagina_Inicio = new Intent(this, Pagina_Inicio.class);
                startActivity(Pagina_Inicio);
                finish();
            }

        } else {

            Splash();

        }
    }

    private void Splash() {


        final Intent Pagina_Login = new Intent(this, LoginActivity.class);

        final SharedPreferences prefs = getSharedPreferences(ToolBox_SharedPrefs.CONSTANTE_SHAREDPREFS_DO_PROJETO, MODE_PRIVATE);
        Boolean splash = prefs.getBoolean(ToolBox_SharedPrefs.CONSTANTE_KEY_SHAREDPREFS_SPLASH, false);
        if (splash != false) {

            startActivity(Pagina_Login);
            finish();

        } else {

            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {

//                            SharedPreferences prefs = getApplicationContext().getSharedPreferences(Nome_Do_SharedPrefs, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(ToolBox_SharedPrefs.CONSTANTE_KEY_SHAREDPREFS_SPLASH, true);
                            editor.commit();

                            startActivity(Pagina_Login);
                            finish();
                        }
                    },
                    3000
            );

        }
    }

}
