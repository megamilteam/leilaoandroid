package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.Activity_Item;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;

import java.util.List;

/**
 * Created by John on 21/11/2017.
 */

public class Adapter_ListView_HistoricoLances extends BaseAdapter {

    private Activity activity;
    private int resource;
    private List<HMAux> dados;
    private View mView;
    private HMAux hmAux = null;

    private LayoutInflater mInflater;

    private ImageView img;

    public interface IAdapterPosts {
        void onStatusChanged(String id, boolean status);
    }

    private IAdapterPosts delegate;

    public void setOnStatusChangedListener(IAdapterPosts delegate) {
        this.delegate = delegate;
    }

    public Adapter_ListView_HistoricoLances(Activity activity, int resource, List<HMAux> dados) {
        this.activity = activity;
        this.resource = resource;
        this.dados = dados;
        this.mInflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux hmAux = dados.get(position);

        return Long.parseLong(hmAux.get(HMAux.ID));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);
        }

        // Acessar os dados de uma posicao especirfica (position)
        hmAux = dados.get(position);

//        hmAux.put(HMAux.ID, String.valueOf(i));// Primary Key
//        hmAux.put(HMAux.ID_LANCE, arrayInterno.getString("id_lance"));
//        hmAux.put(HMAux.NOME_PRODUTO, arrayInterno.getString("nome_produto"));
//        hmAux.put(HMAux.VALOR, arrayInterno.getString("valor"));
//        hmAux.put(HMAux.DATA, arrayInterno.getString("dados"));

        // Acessar a Layout Celula

        TextView data = (TextView)
                convertView.findViewById(R.id.data);

        TextView valor = (TextView)
                convertView.findViewById(R.id.valor);

        TextView nome = (TextView)
                convertView.findViewById(R.id.nome);

        // Juntar Dados / Layout
        data.setText(hmAux.get(HMAux.DATA));
        valor.setText(hmAux.get(HMAux.VALOR));
        nome.setText(hmAux.get(HMAux.NOME_PRODUTO));

        mView = convertView;

        return convertView;
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator upAnim = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    upAnim.setDuration(200);
                    upAnim.setInterpolator(new DecelerateInterpolator());
                    upAnim.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator downAnim = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    downAnim.setDuration(200);
                    downAnim.setInterpolator(new AccelerateInterpolator());
                    downAnim.start();
                    break;
            }
            return false;
        }


    };

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

//            hmAux = dados.get(position);

            String id_ = hmAux.get(HMAux.ID_PROCUTO);
            Log.w("lv", "click " + id_);

            Intent intent = new Intent(activity, Activity_Item.class);
            intent.putExtra(HMAux.ID, id_);

//            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");

            activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation
                    (activity, img, "shareView").toBundle());
        }
    };

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            String id_ = hmAux.get(HMAux.ID_PROCUTO);

            MsgUtil.Funcao_MSG(activity, "Long " + id_);
            return true;
        }
    };


}
