package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.gson.Gson;

import net.megamil.leilao24h.Project.ProjectDialog;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_ListView_Frag_MeusLeiloes;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Menu_Ativar_Leilao.Activity_Ativar_Leilao;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;
import net.megamil.library.BaseActivity.DebugActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class CarteiraVirtualActivity extends DebugActivity implements Adapter_ListView_Frag_MeusLeiloes.IMeuleilaoClick {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private static ImageButton btn_voltar;
    private static ImageButton btn_refresh;
    private  ImageButton btn_trash;
    public  static TextView edt_quantidade_leiloes;
    public  static TextView edt_quantidade_produtos;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    /////////////////////////////////////////////////////////////////////////////
    public static List<String> productsExcluir = new ArrayList<>();
    HashSet<String> selectedProducts= new HashSet<String>();
    private  ConstraintLayout cl_trash;
    private  TextView tv_trash;

    private static Adapter_ListView_Frag_MeusLeiloes.IMeuleilaoClick iMeuleilaoClick;

    /*
     *  Inicio de Variaveis
     */

    private static ListView lv_itens;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escritorio_virtual_listview);

       /* initWSConection();
        initVars();
        initActions();*/
        iMeuleilaoClick = this;

    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh(){
        initWSConection();
        initVars();
        initActions();
    }

    private void initVars() {

        //Var padroes
        context = getApplicationContext();
        context_activity = this;
        activity = this;
        window = getWindow();
        btn_voltar = findViewById(R.id.btn_voltar);
        btn_refresh = findViewById(R.id.btn_refresh);
        btn_trash = findViewById(R.id.btn_trash);
        cl_trash = findViewById(R.id.cl_trash);
        tv_trash = findViewById(R.id.tv_trash);

        edt_quantidade_leiloes = findViewById(R.id.edt_quantidade_leiloes);
        edt_quantidade_produtos = findViewById(R.id.edt_quantidade_produtos);

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        lv_itens = findViewById(R.id.lv_itens);


        edt_quantidade_leiloes.setText(HMAux.QUANTIDADE_LEILAO);
        edt_quantidade_produtos.setText(HMAux.QUANTIDADE_PRODUTOS);
        
    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */
        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        btn_trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(activity)
                        .setMessage("Deseja apagar produtos selecionados")
                        .setPositiveButton("Sim",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteWSConection(selectedProducts);
                                    }
                                }
                        )
                        .setNegativeButton("Não", null)
                        .create()
                        .show();
            }
        });


//        arrayList_dados = gerarDados(90);
//        Set_Dados_Array(arrayList_dados);


    }

    public static void Set_Dados_Array() {

//        arrayList_dados.add(arrayList);

        Adapter_ListView_Frag_MeusLeiloes adapter_leiloes = new Adapter_ListView_Frag_MeusLeiloes(
                activity,
                R.layout.celula_meu_leiloes,
                arrayList_dados,iMeuleilaoClick);

        lv_itens.setAdapter(adapter_leiloes);

        if(activity instanceof CarteiraVirtualActivity) {
            HashSet<String> selectedBooks = new HashSet<String>();

            adapter_leiloes.iMeuleilaoClick.onClickSelect(selectedBooks);
        }



        lv_itens.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long id) {


                return false;
            }
        });

        lv_itens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HMAux hmAux = (HMAux) adapterView.getItemAtPosition(i);

                Intent intent = new Intent(activity, MeusLeilaoArrematadosActivity.class);

                intent.putExtra(HMAux.ID_POSITION, i);
                intent.putExtra(HMAux.INTENT_TYPE, 0);

                intent.putExtra(HMAux.ID_LEILAO, hmAux.get(HMAux.ID_LEILAO));
                intent.putExtra(HMAux.ID_USUARIO, hmAux.get(HMAux.ID_USUARIO));
                intent.putExtra(HMAux.ID_PRODUTO, hmAux.get(HMAux.ID_PRODUTO));
                intent.putExtra(HMAux.NOME_PRODUTO, hmAux.get(HMAux.NOME_PRODUTO));
                intent.putExtra(HMAux.NOME_USUARIO, hmAux.get(HMAux.NOME_USUARIO));
                intent.putExtra(HMAux.NOME_USUARIO_PRODUTO, hmAux.get(HMAux.NOME_USUARIO_PRODUTO));
                intent.putExtra(HMAux.NOME_USUARIO, hmAux.get(HMAux.NOME_USUARIO));
                intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux.get(HMAux.DESCRICAO_PRODUTO));
                intent.putExtra(HMAux.VALOR_MINIMO, hmAux.get(HMAux.VALOR_MINIMO));
                intent.putExtra(HMAux.LANCE_MINIMO, hmAux.get(HMAux.LANCE_MINIMO));
                intent.putExtra(HMAux.INGRESSOS, hmAux.get(HMAux.INGRESSOS));

                intent.putExtra(HMAux.AGENCIA, hmAux.get(HMAux.AGENCIA));
                intent.putExtra(HMAux.CONTA, hmAux.get(HMAux.CONTA));
                intent.putExtra(HMAux.BANCO, hmAux.get(HMAux.BANCO));

                intent.putExtra(HMAux.BAIRRO_USUARIO, hmAux.get(HMAux.BAIRRO_USUARIO));
                intent.putExtra(HMAux.LOGRADOURO_USUARIO, hmAux.get(HMAux.LOGRADOURO_USUARIO));
                intent.putExtra(HMAux.CIDADE_USUARIO, hmAux.get(HMAux.CIDADE_USUARIO));
                intent.putExtra(HMAux.ESTADO_USUARIO, hmAux.get(HMAux.ESTADO_USUARIO));
                intent.putExtra(HMAux.RASTREIO, hmAux.get(HMAux.RASTREIO));
                intent.putExtra(HMAux.REFERENCIA, hmAux.get(HMAux.REFERENCIA));
                intent.putExtra(HMAux.CEP_USUARIO, hmAux.get(HMAux.CEP_USUARIO));

                intent.putExtra(HMAux.DATA_INICIO, hmAux.get(HMAux.DATA_INICIO));
                intent.putExtra(HMAux.DATA_FIM_PREVISTO, hmAux.get(HMAux.DATA_FIM_PREVISTO));
                intent.putExtra(HMAux.STATUS_LEILAO, hmAux.get(HMAux.STATUS_LEILAO));
                intent.putExtra(HMAux.VALOR, hmAux.get(HMAux.VALOR));
                intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux.get(HMAux.IMAGENS_ARRAY));

                ToolBox_Chama_Activity.Funcao_Chama_Activity_For_Result(activity, null, intent,101, false, false, "");

            }
        });



    }

    public void  deleteWSConection(HashSet<String> selected) {
        Gson gson = new Gson();
        Map<String, String> Dados_Para_Parametros = new HashMap<>();
        Dados_Para_Parametros.put("id_produtos",  selected.toString());
        Dados_Ws(28, "", Dados_Para_Parametros, this);
    }


    public void initWSConection() {

        Dados_Ws(29, "", null, this);
    }
    @Override
    public void onBackPressed() {
//        ToolBox_Chama_Activity.Funcao_Chama_TelaPrincipal(activity);
        super.onBackPressed();
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            if(requestCode == 101) {
             initWSConection();
            }
        }
    }

    @Override
    public void onClickSelect(HashSet<String> selected) {
        this.selectedProducts = selected;
        if(selected.size() > 0){
            cl_trash.setVisibility(View.VISIBLE);
            tv_trash.setText(String.valueOf(selected.size()));
        }else{
            selectedProducts.clear();
            cl_trash.setVisibility(View.GONE);
        }
    }
}
