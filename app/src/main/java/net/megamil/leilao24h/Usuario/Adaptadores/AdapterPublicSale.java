/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.megamil.leilao24h.BuildConfig;
import net.megamil.leilao24h.Project.ProjectUtils;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Project.ProjectActivity;
import net.megamil.leilao24h.Project.ProjectDialog;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import maripoppis.com.connection.Constants.ConnectionUrl;
import maripoppis.com.connection.Model.SubModels.Leiloes;


@SuppressWarnings("all")
public class AdapterPublicSale extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //==============================================================================================
    //
    // ** Vars
    //
    //==============================================================================================

    //Type
    private boolean recyclerHorizontal;

    //Pagination
    private int pageNumber = 1;

    //Create adapter
    private Activity activity; // Context
    private List<Leiloes> database = new ArrayList<>(); // List Dados referenciado
    private LayoutInflater inflater;

    //Views Type
    private final int VIEW_SALE = 1;

    //==============================================================================================
    //
    // ** Constructor
    //
    //==============================================================================================

    public AdapterPublicSale(Activity activity, RecyclerView recyclerView, boolean horizontalScroll) {
        recyclerHorizontal = horizontalScroll;
        this.activity = activity;
        this.inflater = LayoutInflater.from(activity);

        if (recyclerHorizontal) {
            recyclerView.setLayoutManager(ToolBox_RecyclerView.Funcao_GridLayoutManager(activity));
        } else {
            recyclerView.setLayoutManager(ToolBox_RecyclerView.Funcao_LinearLayoutManager(activity, false));
        }
        recyclerView.addOnScrollListener(scrollListener);
    }

    //==============================================================================================
    //
    // ** onCreateViewHolder
    //
    //==============================================================================================

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_SALE) {
            return new SaleViewHolder(getView(recyclerHorizontal ? R.layout.celula_leilao : R.layout.cell_publicsale_vertical, parent));
        } else {
            return new EndViewHolder(getView(recyclerHorizontal ? R.layout.celula_recycler_end : R.layout.cell_endview_vertical, parent));
        }
    }

    //==============================================================================================
    //
    // ** Adapter Config Utils
    //
    //==============================================================================================

    @Override
    public int getItemCount() {
        return database.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (database.get(position).getIdLeilao().equals(idEndView) || database.get(position).getIdLeilao().equals(idLoadView)) {
            return 0;
        } else {
            return VIEW_SALE;
        }
    }

    //==============================================================================================
    //
    // ** onBindViewHolder
    //
    //==============================================================================================

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        debugLog(database.get(position).getIdLeilao());

        if (holder instanceof SaleViewHolder) {
            SaleViewHolder viewHolder = (SaleViewHolder) holder;
            viewHolder.bindViewData(database.get(position), position);
            viewHolder.mView.setOnTouchListener(touchListener);
            viewHolder.mView.setTag(position);
        }

        if (holder instanceof EndViewHolder) {
            EndViewHolder viewHolder = (EndViewHolder) holder;
            viewHolder.bindViewData(database.get(position));
            viewHolder.mView.setOnTouchListener(touchListener);
        }

    }

    //==============================================================================================
    //
    // ** GetView
    //
    //==============================================================================================

    private View getView(int cellForInflate) {
        return inflater.inflate(cellForInflate, null);
    }

    private View getView(int cellForInflate, ViewGroup parent) {
        return inflater.inflate(cellForInflate, parent, false);
    }

    //==============================================================================================
    //
    // ** Custon View Params
    //
    //==============================================================================================

    private View setParams(View view) {
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return view;
    }

    //==============================================================================================
    //
    // ** Touch Event
    //
    //==============================================================================================

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator animatorUP = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    animatorUP.setDuration(200);
                    animatorUP.setInterpolator(new DecelerateInterpolator());
                    animatorUP.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator animatorDOWN = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    animatorDOWN.setDuration(200);
                    animatorDOWN.setInterpolator(new AccelerateInterpolator());
                    animatorDOWN.start();
                    break;
            }
            return false;
        }
    };

    //==============================================================================================
    //
    // ** Scroll Listener
    //
    //==============================================================================================

    //load
    private boolean loading;
    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (recyclerHorizontal) {
                final GridLayoutManager layout = (GridLayoutManager) recyclerView.getLayoutManager();
                assert layout != null;
                if (!loading && layout.getItemCount() == (layout.findLastVisibleItemPosition() + 1)) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callScrollListener(pageNumber);
                            debugLog("Scroll Callback");
                        }
                    }, 1500);
                    scrollStop();
                }
            } else {
                final LinearLayoutManager layout = (LinearLayoutManager) recyclerView.getLayoutManager();
                assert layout != null;
                if (!loading && layout.getItemCount() == (layout.findLastVisibleItemPosition() + 1)) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callScrollListener(pageNumber);
                            debugLog("Scroll Callback");
                        }
                    }, 1500);
                    scrollStop();
                }
            }

        }
    };

    private void scrollStop() {
        loading = true;
    }

    private void scrollContinue() {
        loading = false;
    }
    // ** Interface Scroll
    //==============================================================================================

    private void callScrollListener(int pageNumber) {
        if (scrollListenerCallcabk != null) {
            scrollListenerCallcabk.scrollCallback(pageNumber);
        }
    }

    public void addPaginationScrollListener(ScrollPaginationListener scrollListenerCallcabk) {
        this.scrollListenerCallcabk = scrollListenerCallcabk;
    }

    private ScrollPaginationListener scrollListenerCallcabk;

    public interface ScrollPaginationListener {
        void scrollCallback(int pageNumber);
    }


    //==============================================================================================
    //
    // ** Utils for Add and Remove data on Adapter
    //
    //==============================================================================================

    public void add(Leiloes leilao) {
        database.add(leilao);
        notifyItemInserted(database.size() - 1);
    }

    public void remove(int position) {
        database.remove(position);
//        recycler.removeViewAt(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, database.size());
        debugLog("Item removed at position " + position);
    }

    public void addItems(List<Leiloes> data) {

//        if (pageNumber > 2) {
//            data.clear();
//        }
        if (data != null)
            if (data.size() == 0) {
                addEndView();
            } else {
                debugLog("New data List Size: " + data.size());
                removeLoadView();
                database.addAll(data);
                debugLog("Database Size: " + database.size());
                addLoadView();
                scrollContinue();
                notifyDataSetChanged();
                nextPagination();
            }
    }

    public void removeAll() {
        debugLog("Database size is " + database.size() + ", init All Remove");
        database.clear();
        notifyDataSetChanged();
        debugLog("All item removed, Database size is " + database.size());
    }

    //==============================================================================================
    //
    // ** Layout Cell Utils
    //
    //==============================================================================================

    private String idEndView = "endview_added";

    private void addEndView() {
        removeLoadView();
        Leiloes l = new Leiloes();
        l.setIdLeilao(idEndView);
        database.add(l);
        notifyItemInserted(database.size() - 1);
    }

    private void removeEndView() {
        removeLoadView();
        addLoadView();
    }

    private String idLoadView = "loadview_added";

    private void addLoadView() {
        Leiloes l = new Leiloes();
        l.setIdLeilao(idLoadView);
        database.add(l);
        notifyItemInserted(database.size() - 1);
    }

    private void removeLoadView() {
        if (database.size() > 0)
            database.remove(database.size() - 1);
        notifyItemRemoved(database.size());
    }

    //==============================================================================================
    //
    // ** Update Status Favorite in DataBase
    //
    //==============================================================================================

    public void updateStatusFavorite(String favoriteStatus, int position) {
        debugLog("New status Favorite is: " + favoriteStatus);
        database.get(position).setFavorito(favoriteStatus);
    }

    //==============================================================================================
    //
    // ** Sale Holder
    //
    //==============================================================================================

    private class SaleViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        // view
        private ImageView img_item;
        private TextView name_item;
        private TextView tvLabelNew;
        private TextView desc_item;
        private TextView ingressos;
        private TextView tv_title_data_inicio;
        private TextView data_inicio;
        private RelativeLayout rl_img;
        private TextView cidade_usuario;

        private SaleViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            img_item = itemView.findViewById(R.id.img_item);
            name_item = itemView.findViewById(R.id.name_item);
            tvLabelNew = itemView.findViewById(R.id.tvLabelNew);
            desc_item = itemView.findViewById(R.id.desc_item);
            ingressos = itemView.findViewById(R.id.ingressos);
            tv_title_data_inicio = itemView.findViewById(R.id.tv_title_data_inicio);
            data_inicio = itemView.findViewById(R.id.data_inicio);
            rl_img = itemView.findViewById(R.id.rl_img);
            cidade_usuario = itemView.findViewById(R.id.tv_cidade);

        }

        void bindViewData(final Leiloes leiloes, final int position) {

           if(leiloes.getImagens() != null && leiloes.getImagens().size() > 0) {
               ToolBox_Imagem.initGlide(activity, ConnectionUrl.MakeUrlImage(leiloes.getImagens().get(0)), img_item);
           }
            name_item.setText(leiloes.getNomeProduto().trim());
            desc_item.setText(leiloes.getDescricaoProduto());
            ingressos.setText(leiloes.getIngressos());
            try{
                cidade_usuario.setText(leiloes.getCidadeUsuario().trim());

            }catch (Exception e){}

            tvLabelNew.setText(ProjectUtils.verifyNewOrUsed(leiloes.getProdutoUsado()));

            ProjectUtils.verifyInitPublicSale(activity,
                    tv_title_data_inicio,
                    data_inicio,
                    leiloes.getDataInicio(),
                    leiloes.getDataFimPrevisto());

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
                    int myCredits = prefs.getCreditsNum();
                    int creditsToEnter = Integer.parseInt(leiloes.getIngressos());

                    //if (myCredits < creditsToEnter) {
                     //  ProjectDialog.openMoreCredits(activity);
                    //} else {
                        leiloes.setPosition(position);
                        ProjectActivity.goPublicSaleActivity(activity, leiloes);
                       // ProjectActivity.goPublicSaleActivityByNotification(activity, "8");
                 //}
                }
            });

            mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ProjectDialog.showDetails(activity, leiloes, getAdapterPosition());
                    return true;
                }
            });

        }
    }


    //==============================================================================================
    //
    // ** End Holder
    //
    //==============================================================================================

    private class EndViewHolder extends RecyclerView.ViewHolder {
        private View mView;

        private EndViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void bindViewData(final Leiloes leiloes) {
            if (leiloes.getIdLeilao().equals(idEndView)) {
                mView.findViewById(R.id.btnRetry).setOnClickListener(endViewHolderClick);
                mView.findViewById(R.id.endView).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.loadView).setVisibility(View.GONE);
            } else {
                mView.findViewById(R.id.loadView).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.endView).setVisibility(View.GONE);
            }
        }

        private View.OnClickListener endViewHolderClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.findViewById(R.id.loadView).setVisibility(View.VISIBLE);
                tryLoad(pageNumber);
            }
        };

    }


    // ** Interface Try Load
    //==============================================================================================

    private void tryLoad(int pageNumber) {
        if (onTryLoadListener != null)
            onTryLoadListener.tryLoad(pageNumber);
    }

    private OnTryLoadListener onTryLoadListener;

    public void setOnTryLoadListener(OnTryLoadListener onTryLoadListener) {
        this.onTryLoadListener = onTryLoadListener;
    }

    public interface OnTryLoadListener {
        void tryLoad(int pageNumber);
    }

    //==============================================================================================
    //
    // ** Paginations Utils
    //
    //==============================================================================================

    private void nextPagination() {
        pageNumber++;
    }

    private void backPagination() {
        pageNumber--;
    }

    public int getFirstPagination() {
        return pageNumber = 1;
    }

    public void setPaginationNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPaginationNumber() {
        return pageNumber;
    }

    //==============================================================================================
    //
    // ** Utils
    //
    //==============================================================================================

    private void debugLog(String breakText) {
        if (BuildConfig.DEBUG) {
            Log.w(getClass().getSimpleName(), breakText);
        }
    }

    //==============================================================================================
    //
    // ** Guard List Fragment
    //
    //==============================================================================================

    private int paginationNumberTemp = 0;

    public int getPaginationNumberTemp() {
        return paginationNumberTemp;
    }

    public void setPaginationNumberTemp(int paginationNumberTemp) {
        this.paginationNumberTemp = paginationNumberTemp;
    }

    // **
    //==============================================================================================

    private List<Leiloes> databaseTemp = new ArrayList<>();

    public List<Leiloes> getDatabaseTemp() {
        return databaseTemp;
    }

    public void setDatabaseTemp(List<Leiloes> list) {
        databaseTemp.addAll(list);
    }

    public void clearDatabaseTemp() {
        databaseTemp.clear();
    }


}
