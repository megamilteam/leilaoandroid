/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/10/9 at 1:52:58 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Usuario

import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import maripoppis.com.connection.Connection
import maripoppis.com.connection.Model.SubModels.Categorias
import maripoppis.com.connection.Model.WSResponse
import net.megamil.leilao24h.R
import net.megamil.leilao24h.Usuario.Adaptadores.AdapterPublicSale

class Pagina_Lista_leiloes : DebugActivity(), Connection.ConnectCallback {
    private var adapter: AdapterPublicSale? = null
    private var categoria: Categorias.Categoria? = null
    private var refresh: SwipeRefreshLayout? = null
    private var fkCategory: String? = null
    private var recyclerView: RecyclerView? = null
    //connection
    private var connection: Connection? = null
    //Scroll Listenner
    var sizePerPage = 20
    var pageNumber = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categoria_leiloes)

        recyclerView = findViewById(R.id.listItensCategoria)
        refresh = findViewById(R.id.refresh)
        val btnBack: ImageButton = findViewById(R.id.btnBack)
        val title: TextView = findViewById(R.id.tv_toolbar)

        btnBack.setOnClickListener { onBackPressed() }

        if (intent.hasExtra("categoria")) {
            categoria = intent.getSerializableExtra("categoria") as Categorias.Categoria?
            fkCategory = categoria?.idCategoria ?: "1"
            title.text = categoria?.nomeCategoria
           // initRefresh()
            // initList()
           // initWSConection(pageNumber)
        }
    }

    override fun onResume() {
        super.onResume()
         if(categoria != null) {
             initRefresh()
             initList()
             initWSConection(pageNumber)
         }
    }

    private fun initList() {
        adapter = AdapterPublicSale(
            this,
            recyclerView,
            true
        )
        recyclerView?.adapter = adapter

        adapter?.addPaginationScrollListener { pageNumber -> initWSConection(pageNumber) }

        adapter?.setOnTryLoadListener { pageNumber -> initWSConection(pageNumber) }
    }

    private fun initWSConection(pageNumber: Int) {
        Connection.setCallback(this)
        connection = Connection(this)
        connection?.getPublicSaleByCategory(fkCategory, pageNumber)
    }

    //==============================================================================================
    //
    // ** Refresh
    //
    //==============================================================================================
    private fun initRefresh() {
        refresh?.setColorSchemeResources(R.color.colorPrimary)
        refresh?.setOnRefreshListener {
            adapter?.removeAll()
            adapter?.firstPagination
            showLoading()
            initWSConection(0)
        }
        showLoading()
    }

    private fun showLoading() {
        refresh?.isRefreshing = true
    }

    private fun hideRefresh() {
        refresh?.isRefreshing = false
    }

    private var firstLoad = true
    override fun ConnectionSuccess(response: WSResponse?, connectionTypeID: Connection.ConnectionType?) {
        if (connectionTypeID == Connection.ConnectionType.WS_CATEGORIAS_LEILOES) {
            response?.leiloes?.size?.let {
                if (firstLoad) {
                    if (it > 0) {
                        firstLoad = false
                        adapter?.addItems(response.leiloes)
                    } else {
                        Toast.makeText(this, "Nada aqui", Toast.LENGTH_LONG).show()
                        finish()
                    }
                } else {
                    adapter?.addItems(response.leiloes)
                }
            } ?: run { finish() }
        }
        hideRefresh()
    }

    override fun ConnectionError(statusType: Int, response: WSResponse?, connectionTypeID: Connection.ConnectionType?) {
        hideRefresh()
    }

    override fun ConnectionFail(t: Throwable?) {
        hideRefresh()
    }
}
