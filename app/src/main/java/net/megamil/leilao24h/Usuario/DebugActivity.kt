package net.megamil.leilao24h.Usuario

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

open abstract class DebugActivity : AppCompatActivity() {

    // Retorna o nome da classe sem o pacote
    val className: String
        get() {
            val cls = (this as Any).javaClass
            return cls.simpleName
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "$className.onCreate(): $savedInstanceState")
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "$className.onStart().")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i(TAG, "$className.onRestart().")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i(TAG, "$className.onSaveInstanceState().")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "$className.onStop().")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "$className.onDestroy().")
    }

    companion object {
        val TAG = "Plataforma"
    }
}