/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/10/8 at 4:5:26 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Usuario.Adaptadores

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.celula_categoria_home.view.*
import maripoppis.com.connection.BuildConfig
import maripoppis.com.connection.Model.SubModels.Categorias
import net.megamil.leilao24h.R
import net.megamil.leilao24h.Usuario.Pagina_Lista_leiloes
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Chama_Activity
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Imagem
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RecyclerView

class AdapterCategory(
    private val activity: Activity,
    recyclerView: RecyclerView?
) : RecyclerView.Adapter<AdapterCategory.CategoryHolder>() {
    private val database = ArrayList<Categorias.Categoria>()
    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    init {
        recyclerView?.apply {
            layoutManager = ToolBox_RecyclerView.Funcao_GridLayoutManager(activity)!!
            adapter = this@AdapterCategory
        }
    }

    //==============================================================================================
    //
    // ** Touch Event
    //
    //==============================================================================================
    internal var touchListener: View.OnTouchListener = View.OnTouchListener { view, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                val animatorUP = ObjectAnimator.ofFloat(view, "translationZ", 20f)
                animatorUP.duration = 200
                animatorUP.interpolator = DecelerateInterpolator()
                animatorUP.start()
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                val animatorDOWN = ObjectAnimator.ofFloat(view, "translationZ", 0f)
                animatorDOWN.duration = 200
                animatorDOWN.interpolator = AccelerateInterpolator()
                animatorDOWN.start()
            }
        }
        false
    }

    //==============================================================================================
    //
    // ** onCreateViewHolder
    //
    //==============================================================================================
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder =
        CategoryHolder(getView(R.layout.celula_categoria_home, parent))

    override fun getItemCount(): Int = database.size
    override fun getItemViewType(position: Int): Int = database.size
    override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
        holder.bindViewData(database[position])
        holder.mView.setOnTouchListener(touchListener)
        holder.mView.tag = position
    }

    //==============================================================================================
    //
    // ** GetView
    //
    //==============================================================================================
    private fun getView(cellForInflate: Int, parent: ViewGroup): View {
        return inflater.inflate(cellForInflate, parent, false)
    }

    //==============================================================================================
    //
    // ** Custon View Params
    //
    //==============================================================================================
    fun addItems(data: List<Categorias.Categoria>?) {
        data?.let {
            if (it.isNotEmpty()) {
                database.clear()
                database.addAll(data)
                notifyDataSetChanged()
            }
        }
    }

    //==============================================================================================
    //
    // ** Sale Holder
    //
    //==============================================================================================
    inner class CategoryHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        internal fun bindViewData(category: Categorias.Categoria) {
            Log.e("Lista de banner", category.toString())

            ToolBox_Imagem.initGlide(activity,  BuildConfig.CONECTION_BASE_URL+"/upload/categorias/" + category.icone, mView.img_item)

            mView.name_item.text = category.nomeCategoria
            mView.setOnClickListener {
                val intent = Intent(activity, Pagina_Lista_leiloes::class.java)
                intent.putExtra("categoria", category)

                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "")
            }
        }
    }
}