/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/2/2 at 2:0:27 for quantic heart studios
 *
 */

package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.leilao24h.Usuario.Adaptadores.ViewPager_IMG;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Carossel;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Sistema;
import net.megamil.library.BaseActivity.DebugActivity;
import net.megamil.library.EditTextMoney;

import java.util.HashMap;
import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;
import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.Funcao_Converter_Dinhero_Ws;

import static net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos.tGetText;

public class MeusLeilaoArrematadosActivity extends DebugActivity {

    //==============================================================================================
    //
    // ** Variables
    //
    //==============================================================================================

    //init
    private static Activity activity;

    //TextViews
    private TextView tv_title;
    private TextView tv_status;
    private TextView tvLabelAnser;
    private TextView tv_descricao;

    //EditText
    private EditText et_descricao;
    private EditText et_ingressos;
    private EditText et_altura;
    private EditText et_peso;
    private EditText et_largura;
    private EditText et_comprimento;

    private EditText edt_nome;
    private EditText edt_endereco;
    private EditText edt_cidade;
    private EditText edt_estado;
    private EditText edt_cep;
    private EditText edt_bairro;
    private EditText edt_referencia;
    private EditText edt_rastreio;

    private EditText edt_banco;
    private EditText edt_agencia;
    private EditText edt_conta;
    //CurrencyEditText
    private static EditTextMoney et_valor_minimo;
    private static EditTextMoney et_lance_minimo;

    //ImageButton
    private ImageButton btn_close;

    //Button
    private Button btn_iniciar;
    private Button btn_editar;
    //String
    private String ID_PRODUTO = "";
    private String ID_LEILAO = "";

    //Carossel
    private LinearLayout dotsLinearLayout;
    private TextView imgCount;
    private TextView tv_valor;
    private ViewPager mViewPager;
    private RelativeLayout rlControlImg;

    //LinearLayout
    private LinearLayout llControl;
    private LinearLayout llCorreios;
    private LinearLayout llCorreiosCampos;

    private LinearLayout llEditarLance;
    private LinearLayout llExcluirLance;

    //Switch
    private Switch swFrete;
    private String NOME_PRODUTO;
    private String DESCRICAO_PRODUTO;
    private String VALOR_MINIMO;
    private String IMAGENS;
    private String VALOR;
    private String LANCE_MINIMO;
    private String INGRESSOS;
    private String BANCO;
    private String AGENCIA;
    private String CONTA;
    private String BAIRRO_USUARIO;
    private String NOME_USUARIO_PRODUTO;
    private String RASTREIO;
    private String REFERENCIA;
    private String ENDERECO;
    private String CIDADE;
    private String BAIRRO;
    private String ESTADO;
    private String CEP;

    //==============================================================================================
    //
    // ** Create
    //
    //==============================================================================================

    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leilao_arrematados);

        activity = MeusLeilaoArrematadosActivity.this;

        initVars();
        initActions();
        iniDados();
    }

    //==============================================================================================
    //
    // ** Init Methods
    //
    //==============================================================================================

    private void initVars() {

        //linearlayout
        llControl = findViewById(R.id.llControl);
       // llCorreios = findViewById(R.id.llCorreios);
       // llCorreiosCampos = findViewById(R.id.llCorreiosCampos);

        //Switch
       // swFrete = findViewById(R.id.swFrete);

        //textviews
        tv_title = findViewById(R.id.tv_title_produto);
        tv_valor = findViewById(R.id.tv_valor_produto);
        tv_descricao = findViewById(R.id.tv_descricao_produto);
        //tv_status = findViewById(R.id.tv_status);
        tvLabelAnser = findViewById(R.id.tvLabelAnser);

        //edittext
     //   et_descricao = findViewById(R.id.et_descricao);
        edt_nome = findViewById(R.id.edt_nome);
        edt_endereco = findViewById(R.id.edt_endereco);
        edt_cidade = findViewById(R.id.edt_cidade);
        edt_estado = findViewById(R.id.edt_estado);
        edt_cep = findViewById(R.id.edt_cep);
        edt_bairro = findViewById(R.id.edt_bairro);
        edt_referencia = findViewById(R.id.edt_referencia);
        edt_rastreio = findViewById(R.id.edt_rastreio);
        edt_agencia = findViewById(R.id.edt_agencia);
        edt_banco = findViewById(R.id.edt_banco);
        edt_conta= findViewById(R.id.edt_conta);

        // et_valor_minimo = findViewById(R.id.et_valor_minimo);
      //  et_lance_minimo = findViewById(R.id.et_lance_minimo);
       // et_ingressos = findViewById(R.id.et_ingressos);
        //
       // et_altura = findViewById(R.id.et_altura);
      //  et_peso = findViewById(R.id.et_peso);
       // et_largura = findViewById(R.id.et_largura);
      //  et_comprimento = findViewById(R.id.et_comprimento);

        //imagebutton
        btn_close = findViewById(R.id.btn_close);

        //buttons
        btn_iniciar = findViewById(R.id.btn_iniciar);
        btn_iniciar.setOnClickListener(onClickListener_initArticle);

        //Carossel
        dotsLinearLayout = findViewById(R.id.dotLinear);
        imgCount = findViewById(R.id.img_count);
        mViewPager = findViewById(R.id.vp_img_pager);
        rlControlImg = findViewById(R.id.rl_img);
    }

    private void initActions() {
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void iniDados() {

        if (getIntent().getIntExtra(HMAux.INTENT_TYPE, -1) == 0) {

            ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);
            String id = prefs.getID_USUARIO();

          //  btn_iniciar.setVisibility(View.GONE);
           // llCorreios.setVisibility(View.GONE);
            String ID_USUARIO = getIntent().getStringExtra(HMAux.ID_USUARIO);

            ID_LEILAO = getIntent().getStringExtra(HMAux.ID_LEILAO);
            NOME_PRODUTO = getIntent().getStringExtra(HMAux.NOME_PRODUTO);
            NOME_USUARIO_PRODUTO = getIntent().getStringExtra(HMAux.NOME_USUARIO_PRODUTO);
            DESCRICAO_PRODUTO = getIntent().getStringExtra(HMAux.DESCRICAO_PRODUTO);
            BANCO = getIntent().getStringExtra(HMAux.BANCO);
            AGENCIA = getIntent().getStringExtra(HMAux.AGENCIA);
            CONTA = getIntent().getStringExtra(HMAux.CONTA);
            VALOR = getIntent().getStringExtra(HMAux.VALOR);

            CIDADE = getIntent().getStringExtra(HMAux.CIDADE_USUARIO);
            BAIRRO = getIntent().getStringExtra(HMAux.BAIRRO_USUARIO);
            ESTADO = getIntent().getStringExtra(HMAux.ESTADO_USUARIO);
            CEP = getIntent().getStringExtra(HMAux.CEP_USUARIO);
            ENDERECO = getIntent().getStringExtra(HMAux.LOGRADOURO_USUARIO);
            REFERENCIA = getIntent().getStringExtra(HMAux.REFERENCIA);
            RASTREIO = getIntent().getStringExtra(HMAux.RASTREIO);

            BAIRRO_USUARIO = getIntent().getStringExtra(HMAux.BAIRRO_USUARIO);
            IMAGENS = getIntent().getStringExtra(HMAux.IMAGENS_ARRAY);

            tv_valor.setText(activity.getString(R.string.valor_arrematado,VALOR));
            tv_descricao.setText(DESCRICAO_PRODUTO);
            tv_title.setText(NOME_PRODUTO);

            // setTextWithClickableEditText(et_descricao, DESCRICAO_PRODUTO);

            String[] Array_imagens = IMAGENS.split(",");

            ViewPager_IMG mCustomPagerAdapter = new ViewPager_IMG(this, Array_imagens);

            mViewPager.setAdapter(mCustomPagerAdapter);

            new ToolBox_Carossel(
                    activity,
                    dotsLinearLayout,
                    mViewPager,
                    imgCount,
                    Array_imagens.length
            );

            if(ID_USUARIO.equals(id)){
                getFormulario(false,true);
            }else{
                getFormulario(true,false);
            }

        } else if (getIntent().getIntExtra(HMAux.INTENT_TYPE, -1) == 2) {

            btn_iniciar.setVisibility(View.VISIBLE);
            llControl.setVisibility(View.GONE);

            tvLabelAnser.setText(getString(R.string.por_quanto_quer_vender));
            tvLabelAnser.setVisibility(View.VISIBLE);
            ID_LEILAO = getIntent().getStringExtra(HMAux.ID_LEILAO);

            ID_PRODUTO = getIntent().getStringExtra(HMAux.ID_PROCUTO);
            String NOME_PRODUTO = getIntent().getStringExtra(HMAux.NOME_PRODUTO);
            String DESCRICAO_PRODUTO = getIntent().getStringExtra(HMAux.DESCRICAO_PRODUTO);
            String NOME_CATEGORIA = getIntent().getStringExtra(HMAux.NOME_CATEGORIA);
            String IMAGENS = getIntent().getStringExtra(HMAux.IMAGENS_ARRAY);

            tv_title.setText(NOME_PRODUTO);
            et_descricao.setText(DESCRICAO_PRODUTO);
            tv_status.setText(NOME_CATEGORIA);

            String[] Array_imagens = IMAGENS.split(",");

            ViewPager_IMG mCustomPagerAdapter = new ViewPager_IMG(this, Array_imagens);

            mViewPager.setAdapter(mCustomPagerAdapter);

            new ToolBox_Carossel(
                    activity,
                    dotsLinearLayout,
                    mViewPager,
                    imgCount,
                    Array_imagens.length
            );

            btn_iniciar.setOnClickListener(onClickListener_initArticle);

            setActionsSwitchCorreios();
        } else if (getIntent().getIntExtra(HMAux.INTENT_TYPE, -1) == 3) {
//            svLayout.setVisibility(View.GONE);

            rlControlImg.setVisibility(View.GONE);
            llControl.setVisibility(View.GONE);
            ID_LEILAO = getIntent().getStringExtra(HMAux.ID_LEILAO);

            ID_PRODUTO = getIntent().getStringExtra(HMAux.ID_PROCUTO);
            String nomeProduto = getIntent().getStringExtra(HMAux.NOME_PRODUTO);

            tv_title.setText(nomeProduto);
            tv_status.setText(getString(R.string.por_quanto_quer_vender));
            btn_iniciar.setOnClickListener(onClickListener_initArticle);

            setActionsSwitchCorreios();
        }


    }

    public void getFormulario(Boolean isEnabled,Boolean isRastreio){

        edt_nome.setText(NOME_USUARIO_PRODUTO);
        edt_nome.setEnabled(isEnabled);

        edt_cep.setText(CEP);
        edt_cep.setEnabled(isEnabled);

        edt_endereco.setText(ENDERECO);
        edt_endereco.setEnabled(isEnabled);

        edt_cidade.setText(CIDADE);
        edt_cidade.setEnabled(isEnabled);

        edt_estado.setText(ESTADO);
        edt_estado.setEnabled(isEnabled);

        edt_referencia.setText(REFERENCIA);
        edt_referencia.setEnabled(isEnabled);

        edt_rastreio.setText(RASTREIO);
        edt_rastreio.setEnabled(isRastreio);

        edt_bairro.setText(BAIRRO_USUARIO);
        edt_bairro.setEnabled(isEnabled);

        edt_agencia.setText(AGENCIA);
        edt_agencia.setEnabled(isEnabled);
        edt_conta.setText(CONTA);
        edt_conta.setEnabled(isEnabled);

        edt_banco.setText(BANCO);
        edt_banco.setEnabled(isEnabled);

    }

    //==============================================================================================
    //
    // ** Layout correios
    //
    //==============================================================================================

    private void setActionsSwitchCorreios() {
        swFrete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    llCorreiosCampos.setVisibility(View.VISIBLE);
                } else {
                    llCorreiosCampos.setVisibility(View.GONE);
                }
            }
        });
    }

    //==============================================================================================
    //
    // ** Utils
    //
    //==============================================================================================

    private void setTextWithClickableTextViewStatus(TextView textView, String string) {
        if (string.equals("Aprovado")) {
            textView.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        } else {
            textView.setTextColor(activity.getResources().getColor(R.color.google_app_red));
        }
        textView.setText(string);
    }

    private void setTextWithClickableEditText(EditText editText, String string) {
        editText.setEnabled(false);
        editText.setText(string);
    }

    View.OnClickListener onClickListener_initArticle = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            enviarDados();
        }
    };

    //==============================================================================================
    //
    // ** Connections
    //
    //==============================================================================================

    private void initWSConection() {

        if (ToolBox_Verivifacao_Sistema.Funcao_Status_Conexao(activity)) { // função para checar a conexao antes
            String valor_minimo = et_valor_minimo.getText().toString().trim();

            boolean[] Array_De_Obrigatorios;
            boolean ok;
            Map<String, String> Dados_Para_Parametros = new HashMap<>();

            //

            if (swFrete.isChecked()) {
                Array_De_Obrigatorios = new boolean[]{
                        ToolBox_Verivifacao_Campos.verifyFields(activity, et_peso),
                        ToolBox_Verivifacao_Campos.verifyFields(activity, et_comprimento),
                        ToolBox_Verivifacao_Campos.verifyFields(activity, et_largura),
                        ToolBox_Verivifacao_Campos.verifyFields(activity, et_altura),
                        ToolBox_Verivifacao_Campos.verifyFields(activity, et_valor_minimo)
                };
                ok = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);
                Dados_Para_Parametros.put("altura", et_altura.getText().toString().trim());
                Dados_Para_Parametros.put("comprimento", et_comprimento.getText().toString().trim());
                Dados_Para_Parametros.put("peso", et_peso.getText().toString().trim());
                Dados_Para_Parametros.put("largura", et_largura.getText().toString().trim());

            } else {
                Array_De_Obrigatorios = new boolean[]{
                        ToolBox_Verivifacao_Campos.verifyFields(activity, et_valor_minimo)
                };
                ok = ToolBox_Verivifacao_Campos.Funcao_Verifica_Se_Todos_Foram_Preenchidos(Array_De_Obrigatorios);
                Dados_Para_Parametros.put("altura", "0");
                Dados_Para_Parametros.put("comprimento", "0");
                Dados_Para_Parametros.put("peso", "0");
                Dados_Para_Parametros.put("largura", "0");
            }

            if (ok) {

                Dados_Para_Parametros.put("id_produto", ID_PRODUTO);
                Dados_Para_Parametros.put("valor_minimo", Funcao_Converter_Dinhero_Ws(valor_minimo));
                Dados_Ws(9, "", Dados_Para_Parametros, activity);
            }

        } else {
            MsgUtil.Funcao_MSG(activity, activity.getResources().getString(R.string.msg_global_sem_conexao));
        }
    }

    //==============================================================================================
    //
    // ** @Override
    //
    //==============================================================================================

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public static void closeActivity() {
        if (activity != null) {
            activity.finish();
        }
        activity = null;
    }

    public void enviarDados(){

        ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);
        String userID = prefs.getID_USUARIO();

        String Dados_Para_URL_GET = "";
        Map<String, String> Dados_Para_Parametros = new HashMap<>();

        Dados_Para_Parametros.put("fk_usuario", String.valueOf(userID));
        Dados_Para_Parametros.put("nome_usuario", tGetText(edt_nome));
       // Dados_Para_Parametros.put("cpf_usuario", tGetText(et_cpf_usuario));
       // Dados_Para_Parametros.put("celular_usuario", tGetText(et_celular_usuario));
       // Dados_Para_Parametros.put("telefone_usuario", tGetText(et_telefone_usuario));
        Dados_Para_Parametros.put("cep_usuario", tGetText(edt_cep));
        Dados_Para_Parametros.put("logradouro_usuario", tGetText(edt_endereco));
        Dados_Para_Parametros.put("bairro_usuario", tGetText(edt_bairro));
        Dados_Para_Parametros.put("cidade_usuario", tGetText(edt_cidade));
        Dados_Para_Parametros.put("estado_usuario", tGetText(edt_estado));
        Dados_Para_Parametros.put("banco_usuario", tGetText(edt_banco));
        Dados_Para_Parametros.put("agencia_usuario", tGetText(edt_agencia));
        Dados_Para_Parametros.put("conta_usuario", tGetText(edt_conta));
        //Dados_Para_Parametros.put("numero_usuario", tGetText(et_numero_usuario));
        Dados_Para_Parametros.put("complemento_usuario", tGetText(edt_referencia));
        Dados_Para_Parametros.put("codigo_rastreio", tGetText(edt_rastreio));
        Dados_Para_Parametros.put("fk_leilao", ID_LEILAO);
        //Dados_Para_Parametros.put("email_usuario", tGetText(et_email_usuario));


        Dados_Ws(30, Dados_Para_URL_GET, Dados_Para_Parametros, activity);
    }
}
