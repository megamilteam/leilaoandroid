package net.megamil.leilao24h.Usuario.Fragmentos.Perfil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.megamil.leilao24h.Project.ProjectDialog;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.Adapter_ListView_Frag_MeusLeiloes;
import net.megamil.leilao24h.Usuario.Adaptadores.HMAux;
import net.megamil.library.BaseActivity.DebugActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_WS.Dados_Ws;


public class Ativar_Leilao extends DebugActivity implements Adapter_ListView_Frag_MeusLeiloes.IMeuleilaoClick {

    private static Context context;
    private Context context_activity;
    private static Activity activity;
    private Window window;

    private static ImageButton btn_voltar;
    private static ImageButton btn_refresh;
    private  ImageButton btn_trash;
    public static List<HMAux> arrayList_dados = new ArrayList<>();
    /////////////////////////////////////////////////////////////////////////////
    public static List<String> productsExcluir = new ArrayList<>();
    HashSet<String> selectedProducts= new HashSet<String>();
    private  ConstraintLayout cl_trash;
    private  TextView tv_trash;

    private static Adapter_ListView_Frag_MeusLeiloes.IMeuleilaoClick iMeuleilaoClick;

    /*
     *  Inicio de Variaveis
     */

    private static ListView lv_itens;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_listview);

       /* initWSConection();
        initVars();
        initActions();*/
        iMeuleilaoClick = this;

    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh(){
        initWSConection();
        initVars();
        initActions();
    }

    private void initVars() {

        //Var padroes
        context = getApplicationContext();
        context_activity = Ativar_Leilao.this;
        activity = Ativar_Leilao.this;
        window = getWindow();
        btn_voltar = findViewById(R.id.btn_voltar);
        btn_refresh = findViewById(R.id.btn_refresh);
        btn_trash = findViewById(R.id.btn_trash);
        cl_trash = findViewById(R.id.cl_trash);
        tv_trash = findViewById(R.id.tv_trash);

        ////////////////////////////////////////////////////////////////////////

        /*
         *  Inicio de findViewById's
         */

        lv_itens = findViewById(R.id.lv_itens);

    }

    private void initActions() {
        /*
         *  Inicio de Ações da Pagina
         */
        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        btn_trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(activity)
                        .setMessage("Deseja apagar produtos selecionados")
                        .setPositiveButton("Sim",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteWSConection(selectedProducts);
                                    }
                                }
                        )
                        .setNegativeButton("Não", null)
                        .create()
                        .show();
            }
        });


//        arrayList_dados = gerarDados(90);
//        Set_Dados_Array(arrayList_dados);


    }

    public static void Set_Dados_Array() {

//        arrayList_dados.add(arrayList);

        Adapter_ListView_Frag_MeusLeiloes adapter_leiloes = new Adapter_ListView_Frag_MeusLeiloes(
                activity,
                R.layout.celula_leiloes,
                arrayList_dados,iMeuleilaoClick);

        lv_itens.setAdapter(adapter_leiloes);

        if(activity instanceof Ativar_Leilao) {
            HashSet<String> selectedBooks = new HashSet<String>();

            adapter_leiloes.iMeuleilaoClick.onClickSelect(selectedBooks);
        }



        lv_itens.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long id) {

                HMAux hmAux = (HMAux) adapterView.getItemAtPosition(i);
                ProjectDialog.openSelectMenuEditOrCreate(activity, hmAux, i);

                adapter_leiloes.check(hmAux.get(HMAux.ID_PRODUTO));

                return false;
            }
        });

        lv_itens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HMAux hmAux = (HMAux) adapterView.getItemAtPosition(i);
                ProjectDialog.openSelectMenuEditOrCreate(activity, hmAux, i);
            }
        });
    }

    public void  deleteWSConection(HashSet<String> selected) {
        Gson gson = new Gson();
        Map<String, String> Dados_Para_Parametros = new HashMap<>();
        Dados_Para_Parametros.put("id_produtos",  selected.toString());
        Dados_Ws(28, "", Dados_Para_Parametros, Ativar_Leilao.this);
    }


    public void initWSConection() {
        Dados_Ws(8, "", null, Ativar_Leilao.this);
    }
    @Override
    public void onBackPressed() {
//        ToolBox_Chama_Activity.Funcao_Chama_TelaPrincipal(activity);
        super.onBackPressed();
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            if(requestCode == 101) {
             initWSConection();
            }
        }
    }

    @Override
    public void onClickSelect(HashSet<String> selected) {
        this.selectedProducts = selected;
        if(selected.size() > 0){
            cl_trash.setVisibility(View.VISIBLE);
            tv_trash.setText(String.valueOf(selected.size()));
        }else{
            selectedProducts.clear();
            cl_trash.setVisibility(View.GONE);
        }
    }
}
