package net.megamil.leilao24h.Usuario.Fragmentos.Categoria;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import net.megamil.leilao24h.R;

/**
 * Created by John on 09/12/2017.
 */

public class Fragment_2 extends Fragment {

    private Context context;
    private Context context_activity;
    private Activity activity;
    private Window window;

    private static ViewPager viewPager;
    private static TabLayout tabLayout;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_2, container, false);

        initVars(view);
        initActions();

        return view;
    }

    private void initVars(View view) {

        //Var padroes
        context = getActivity();
        context_activity = getActivity();
        activity = getActivity();
        window = getActivity().getWindow();


    }

    private void initActions() {

    }

}
