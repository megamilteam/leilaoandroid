package net.megamil.leilao24h.Usuario.Adaptadores;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Fragmentos.Perfil.Ativar_Leilao;
import net.megamil.leilao24h.Utils.Conexao_Volley.Conexao_Constantes;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;

import java.util.HashSet;
import java.util.List;

/**
 * Created by John on 21/11/2017.
 */

public class Adapter_ListView_Frag_MeusLeiloes extends BaseAdapter {

    private Activity activity;
    private int resource;
    private List<HMAux> dados;
    private View mView;
    private HMAux hmAux = null;
    ViewHolder holder;
    public IMeuleilaoClick iMeuleilaoClick;

    private LayoutInflater mInflater;

    private ImageView img;

    public interface IAdapterPosts {
        void onStatusChanged(String id, boolean status);
    }

    HashSet<String> selectedBooks = new HashSet<String>();

    //This listener will be used on all your checkboxes, there's no need to
    //create a listener for every checkbox.
    CompoundButton.OnCheckedChangeListener checkChangedListener = new CompoundButton.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String bookDuration = (String) buttonView.getTag();
            if(isChecked){
                selectedBooks.add(bookDuration);
            }else{
                selectedBooks.remove(bookDuration);
            }

            if(activity instanceof Ativar_Leilao) {
                iMeuleilaoClick.onClickSelect(selectedBooks);
            }
        }
    };

    public void check(String id_produto){

        boolean bookSelected = false;

        if(!selectedBooks.contains(id_produto)){
            bookSelected = true;
            selectedBooks.add(id_produto);
        }else{
            selectedBooks.remove(id_produto);
        }
        if(activity instanceof Ativar_Leilao) {
            iMeuleilaoClick.onClickSelect(selectedBooks);
        }
        holder.check.setChecked(bookSelected);
    }

    private IAdapterPosts delegate;

    public void setOnStatusChangedListener(IAdapterPosts delegate) {
        this.delegate = delegate;
    }

    public Adapter_ListView_Frag_MeusLeiloes(Activity activity, int resource, List<HMAux> dados,IMeuleilaoClick iMeuleilaoClick) {
        this.activity = activity;
        this.resource = resource;
        this.dados = dados;
        this.mInflater = LayoutInflater.from(activity);

        this.iMeuleilaoClick  = iMeuleilaoClick;
    }

    @Override
    public int getCount() {
        return dados.size();
    }

    @Override
    public Object getItem(int position) {
        return dados.get(position);
    }

    @Override
    public long getItemId(int position) {
        HMAux hmAux = dados.get(position);

        return Long.parseLong(hmAux.get(HMAux.ID));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(resource, parent, false);

            holder = new ViewHolder();

            // Acessar a Layout Celula

             holder.tv_nome = (TextView)
                    convertView.findViewById(R.id.tv_nome);

            holder.tv_descricao = (TextView)
                    convertView.findViewById(R.id.tv_descricao);

            holder.tv_categorias = (TextView) convertView.findViewById(R.id.tv_categorias);

            holder.tv_valor = (TextView) convertView.findViewById(R.id.tv_valor);

            holder.tv_nome_produto = (TextView) convertView.findViewById(R.id.tv_nome_pessoa_produto);

          //  holder.tv_telefone_produto = (TextView) convertView.findViewById(R.id.tv_telefone_produto);

            holder.img = (ImageView)
                    convertView.findViewById(R.id.iv_item);
            holder.check = (CheckBox) convertView.findViewById(R.id.check);

            holder.check.setOnCheckedChangeListener(checkChangedListener);

            if(activity instanceof Ativar_Leilao) {
                holder.check.setVisibility(View.VISIBLE);
            }else{
                holder.check.setVisibility(View.GONE);
            }

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        // Acessar os dados de uma posicao especirfica (position)
        hmAux = dados.get(position);

        // Juntar Dados / Layout
        holder.tv_nome.setText(hmAux.get(HMAux.NOME_PRODUTO));
        holder.tv_descricao.setText(hmAux.get(HMAux.DESCRICAO_PRODUTO));
        holder.tv_categorias.setText(hmAux.get(HMAux.NOME_CATEGORIA));

        if(hmAux.get(HMAux.NOME_USUARIO_PRODUTO) != null) {
            holder.tv_nome_produto.setText(hmAux.get(HMAux.NOME_USUARIO_PRODUTO));
        }

        if(hmAux.get(HMAux.VALOR) != null) {
            holder.tv_valor.setText(hmAux.get(HMAux.VALOR));
        }

        String telefone = hmAux.get(HMAux.TELEFONE_USUARIO_PRODUTO) == null? "": hmAux.get(HMAux.TELEFONE_USUARIO_PRODUTO);

        if(holder.tv_telefone_produto != null) {
            holder.tv_telefone_produto.setText(activity.getString(R.string.telefone_usuario_produto, telefone));
            holder.tv_nome_produto.setText(hmAux.get(HMAux.NOME_USUARIO_PRODUTO));
            holder.tv_valor.setText(activity.getString(R.string.valor_arrematado, hmAux.get(HMAux.VALOR)));
            Linkify.addLinks(holder.tv_valor, Linkify.PHONE_NUMBERS);
           holder.tv_valor.setLinksClickable(true);
    }
        holder.check.setTag(hmAux.get(HMAux.ID_PRODUTO));


        Glide.with(activity).asBitmap()
                .load(Conexao_Constantes.Base_URL_WS + hmAux.get(HMAux.IMAGENS))
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                        .centerCrop()
                )
                .into(holder.img);


        boolean bookSelected = false;

        if(selectedBooks.contains(hmAux.get(HMAux.ID_PRODUTO))){
            bookSelected = true;
        }

        holder.check.setChecked(bookSelected);

        mView = convertView;

        mView.setOnTouchListener(touchListener);
//        mView.setOnClickListener(clickListener);
//        mView.setOnLongClickListener(onLongClickListener);

//        mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                Intent intent = new Intent(activity, Activity_Ativar_Leilao.class);
////
////                intent.putExtra(HMAux.ID_POSITION, position);
////                intent.putExtra(HMAux.INTENT_TYPE, 1);
////
////                intent.putExtra(HMAux.ID_LEILAO, hmAux.get(HMAux.ID_LEILAO));
////                intent.putExtra(HMAux.ID_PROCUTO, hmAux.get(HMAux.ID_PROCUTO));
////                intent.putExtra(HMAux.NOME_PRODUTO, hmAux.get(HMAux.NOME_PRODUTO));
////                intent.putExtra(HMAux.DESCRICAO_PRODUTO, hmAux.get(HMAux.DESCRICAO_PRODUTO));
////                intent.putExtra(HMAux.VALOR_MINIMO, hmAux.get(HMAux.VALOR_MINIMO));
////                intent.putExtra(HMAux.LANCE_MINIMO, hmAux.get(HMAux.LANCE_MINIMO));
////                intent.putExtra(HMAux.INGRESSOS, hmAux.get(HMAux.INGRESSOS));
////                intent.putExtra(HMAux.DATA_INICIO, hmAux.get(HMAux.DATA_INICIO));
////                intent.putExtra(HMAux.DATA_FIM_PREVISTO, hmAux.get(HMAux.DATA_FIM_PREVISTO));
////                intent.putExtra(HMAux.STATUS_LEILAO, hmAux.get(HMAux.STATUS_LEILAO));
////                intent.putExtra(HMAux.IMAGENS_ARRAY, hmAux.get(HMAux.IMAGENS_ARRAY));
////
////
////                ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
//
//            }
//        });

        return convertView;
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    ObjectAnimator upAnim = ObjectAnimator.ofFloat(view, "translationZ", 20);
                    upAnim.setDuration(200);
                    upAnim.setInterpolator(new DecelerateInterpolator());
                    upAnim.start();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    ObjectAnimator downAnim = ObjectAnimator.ofFloat(view, "translationZ", 0);
                    downAnim.setDuration(200);
                    downAnim.setInterpolator(new AccelerateInterpolator());
                    downAnim.start();
                    break;
            }
            return false;
        }


    };

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

//            hmAux = dados.get(position);
//
//            String id_ = hmAux.get(HMAux.ID_PROCUTO);
//            Log.w("lv", "click " + id_);
//
//            Intent intent = new Intent(activity, Activity_Item.class);
//            intent.putExtra(HMAux.ID, id_);
//
////            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
//
//            activity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation
//                    (activity, img, "shareView").toBundle());
        }
    };

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            String id_ = hmAux.get(HMAux.ID_PROCUTO);

            MsgUtil.Funcao_MSG(activity, "Long " + id_);
            return true;
        }
    };


    static class ViewHolder {
        TextView tv_nome;
        TextView tv_descricao;
        TextView tv_categorias;
        TextView tv_valor;
        TextView tv_nome_produto;
        TextView tv_telefone_produto;
        ImageView img;
        CheckBox check;
    }

   public interface IMeuleilaoClick{
        void onClickSelect(HashSet<String> selected) ;
   }
}
