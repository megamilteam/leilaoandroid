package net.megamil.leilao24h.Usuario.Fragmentos.Inicio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import net.megamil.leilao24h.Project.ProjectActivity;
import net.megamil.leilao24h.Project.ProjectDialog;
import net.megamil.leilao24h.Project.ProjectUtils;
import net.megamil.leilao24h.R;

import com.quanticheart.imagescarossel.CarosselImageAdapter;

import net.megamil.leilao24h.Utils.Api_Login.FireBase.LeilaoFirebaseMessagingService;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.NotificationUtils;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Carossel;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Data_e_Hora;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Mascaras;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_RingTone;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_Verivifacao_Campos;
import net.megamil.library.Utils.ActivityUtil;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import maripoppis.com.connection.Connection;
import maripoppis.com.connection.Constants.ConnectionUrl;
import maripoppis.com.connection.Model.SubModels.Leiloes;
import maripoppis.com.connection.Model.WSResponse;
import qunaticheart.com.animations.DialogUtils;
import qunaticheart.com.animations.Loadding;

import static android.view.View.GONE;
import static maripoppis.com.connection.Connection.STATUS_COMPRAR_INGRESSO;
import static maripoppis.com.connection.Connection.STATUS_OK;
import static net.megamil.leilao24h.Utils.Api_Login.FireBase.LeilaoFirebaseMessagingService.FB_NORTIFICATION_ID;
import static net.megamil.leilao24h.Utils.Api_Login.FireBase.LeilaoFirebaseMessagingService.FB_NORTIFICATION_MENSAGEM;
import static net.megamil.leilao24h.Utils.Api_Login.FireBase.LeilaoFirebaseMessagingService.FB_NORTIFICATION_TITULO;
import static net.megamil.leilao24h.Utils.ToolBox.A_ToolBox_Gerador_De_Dados.getSpinnerDataFromID;
import static net.megamil.leilao24h.Utils.ToolBox.MsgUtil.debugLog;

public class PublicSale extends AppCompatActivity implements Connection.ConnectCallback, DialogUtils.OnLoadCancelListener {

    //Vars for Activity
    private Activity activity;

    //==============================================================================================
    //
    // ** Views ID's
    //
    //==============================================================================================

    //Carossel
    @BindView(R.id.vp_img_pager)
    ViewPager mViewPager;
    @BindView(R.id.dotLinear)
    LinearLayout dotsLinearLayout;
    @BindView(R.id.img_count)
    TextView imgCount;

    //TextView
    @BindView(R.id.tv_share_view_tip)
    TextView tv_share_view_tip;
    @BindView(R.id.tv_titulo)
    TextView tv_titulo;
    @BindView(R.id.tv_estado)
    TextView tv_estado;
    @BindView(R.id.valor_principal)
    TextView valor_principal;
    @BindView(R.id.valor_secundario)
    TextView valor_secundario;
    @BindView(R.id.conometro)
    TextView conometro;
    @BindView(R.id.valor_atual)
    TextView valor_atual;
    @BindView(R.id.valor_atualizado)
    TextView valor_atualizado;
    @BindView(R.id.finaliza)
    TextView finaliza;
    @BindView(R.id.descricao)
    TextView descricao;
    @BindView(R.id.leiloero)
    TextView leiloero;
    @BindView(R.id.tvLabelNew)
    TextView tvLabelNew;
    @BindView(R.id.ultimo_lance)
    TextView ultimo_lance;
    @BindView(R.id.credits)
    TextView credits;
    @BindView(R.id.labelEndDate)
    TextView labelEndDate;
    @BindView(R.id.tvLabelChonometer)
    TextView tvLabelChonometer;
    @BindView(R.id.tvMinBid)
    TextView tvMinBid;

    //ImageView
    @BindView(R.id.btn_favorito)
    ImageView btn_favorito;

    //ImageButton
    @BindView(R.id.btn_close)
    ImageButton btn_close;

    //LinearLayout
    @BindView(R.id.btn_historico)
    LinearLayout btn_historico;
    @BindView(R.id.llcredits)
    LinearLayout llcredits;

    //Button
    @BindView(R.id.btn_dar_lance)
    Button btn_dar_lance;

    //RatingBar
    @BindView(R.id.rb_avaliaty)
    RatingBar rb_avaliaty;

    //==============================================================================================
    //
    // ** Socket And Vars
    //
    //==============================================================================================

    private boolean initPerUsers = false;
    private String chamados = "";

    private long dataStartCountDown = 0;
    private long dataEndCountDown = 0;

    //==============================================================================================
    //
    // ** Others
    //
    //==============================================================================================

    //Vars
    private String currentValue;
    private String currentBidValue;
    private String DATA_INICIO;
    private String DATA_FIM_PREVISTO;
    //

    private String id_usuario = "";
    private String id_leilao = "";
    private Handler handle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = PublicSale.this;
        initLoadding();

        setContentView(R.layout.activity_public_sale);
        ButterKnife.bind(this);
        initWSConection();
        verifyBundleData();

        handle = new Handler();
    }

    //==============================================================================================
    //
    // ** Verify Bunble - is Notification or Intent
    //
    //==============================================================================================

    private Leiloes publicSale;

    private void verifyBundleData() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        if (bundle.containsKey(FB_NORTIFICATION_ID)) {
            getPublicSaleFromWS(bundle.getString(FB_NORTIFICATION_ID));
        } else {
            Leiloes publicSale = (Leiloes) bundle.getSerializable(ProjectActivity.KEY_PUBLICSALE);
            assert publicSale != null;
            startActivityByPublicSale(publicSale);
        }

    }

    //==============================================================================================
    //
    // ** Create and init Activity
    //
    //==============================================================================================

    private void startActivityByPublicSale(Leiloes publicSale) {

        if (publicSale != null) {
            this.publicSale = publicSale;
            debugLog("Public sale data is = " + publicSale.toString());
            initActivity();
        } else {
            onBackPressed();
        }
    }

    //==============================================================================================
    //
    // ** Loadding
    //
    //==============================================================================================

    private Loadding loadding;

    private void initLoadding() {
        loadding = new Loadding(activity, PublicSale.this);
        loadding.showLoading();
    }

    private void closeLoading() {
        loadding.closeLoading();
    }

    //==============================================================================================
    //
    // ** Init Activity Functions and Socket
    //
    //==============================================================================================

    private void initActivity() {
        restartVariables();

        setDataInView();
        setActions();
    }

    //==============================================================================================
    //
    // ** Set data from PublicSale mode in View
    //
    //==============================================================================================

    private void setDataInView() {

        // ** Text Only
        //==============================================================================================
        tv_titulo.setText(publicSale.getNomeProduto());
        valor_atual.setText(ToolBox_Mascaras.Funcao_Mascara_Moeda_NumberFormat(publicSale.getValorMinimo()));
        valor_atualizado.setText(ToolBox_Mascaras.Funcao_Mascara_Moeda_NumberFormat(publicSale.getLanceMinimo()));
        descricao.setText(publicSale.getDescricaoProduto());
        leiloero.setText(publicSale.getNomeUsuario());

        // ** Text From Functions
        //==============================================================================================
        tv_estado.setText(getSpinnerDataFromID(publicSale.getEstadoUsuario()));
        credits.setText(new ToolBox_SharedPrefs(activity).getCredits());
        rb_avaliaty.setRating(Float.parseFloat(publicSale.getAvaliacao()));
        tvLabelNew.setText(ProjectUtils.verifyNewOrUsed(publicSale.getProdutoUsado()));
        valor_principal.setText(publicSale.getValorMinimo().split(",")[0].replaceAll("[^\\d.]", ""));

        if (publicSale.getIngressos().equals("0")) {
            tvMinBid.setVisibility(View.VISIBLE);
        } else {
            tvMinBid.setVisibility(GONE);
        }

        // **
        //==============================================================================================
        currentBidValue = publicSale.getLanceMinimo();
        DATA_INICIO = publicSale.getDataInicio();
        DATA_FIM_PREVISTO = publicSale.getDataFimPrevisto();

        if (!ToolBox_Verivifacao_Campos.Funcao_Verifica_Data_Menor_que_Agora(DATA_FIM_PREVISTO)) {
            closeLoading();
            publicSaleFinished();
        } else {
            cashInCraditForPublicSaleEnter();

        }

    }

    //==============================================================================================
    //
    // ** init Vars
    //
    //==============================================================================================

    private void setActions() {

        // ** Block Avaliaty
        //==============================================================================================
        rb_avaliaty.setEnabled(false);

        // ** Click LinearLayout for going Purchase Credits
        //==============================================================================================
        llcredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectActivity.goCreditsPurchaseWithMsg(activity);
            }
        });

        // ** Create Carossel
        //==============================================================================================
        mViewPager.setAdapter(new CarosselImageAdapter(
                activity,
                ProjectUtils.getFinalListUrlImage(publicSale.getImagens())));

        new ToolBox_Carossel(
                activity,
                dotsLinearLayout,
                mViewPager,
                imgCount,
                publicSale.getImagens().size()
        );

        // ** Verify Favorite Status
        //==============================================================================================
        ProjectUtils.verifyFavoriteStatus(btn_favorito, publicSale.getFavorito());
        addPublicSaleInFavoriteList();

        // ** Buttons Events
        //==============================================================================================
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_dar_lance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modalBid();
            }
        });

        // ** History Button Verify


//        showBtnHistory();


    }

    //==============================================================================================
    //
    // ** Favorits Methods
    //
    //==============================================================================================

    private void addPublicSaleInFavoriteList() {
        btn_favorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFavoritStatusView();
                connection.setPublicSaleInFavorite(publicSale.getIdLeilao());
            }
        });
    }

    private void updateFavoritStatusView() {
        publicSale.setFavorito(publicSale.getFavorito().equals("0") ? "1" : "0");
        ProjectUtils.verifyFavoriteStatus(btn_favorito, publicSale.getFavorito());
        if (updateFavoriteStatus != null)
            updateFavoriteStatus.favoriteStatus(publicSale.getFavorito(), publicSale.getPosition());
    }

    // ** Interface Favorite Status Update
    //==============================================================================================

    private static UpdateFavoriteStatus updateFavoriteStatus;

    public static void setUpdateFavoriteStatus(UpdateFavoriteStatus mUpdateFavoriteStatus) {
        updateFavoriteStatus = mUpdateFavoriteStatus;
    }

    @Override
    public void cancelDialog() {
        onBackPressed();
    }

    public interface UpdateFavoriteStatus {
        void favoriteStatus(String favoriteStatus, int position);
    }

    //==============================================================================================
    //
    // ** Reset Variables
    //
    //==============================================================================================

    private void restartVariables() {
        initPerUsers = false;
        chrometerValue = "";
        dataStartCountDown = 0;
        dataEndCountDown = 0;
    }

    //==============================================================================================
    //
    // ** Sockets Functions
    //
    //==============================================================================================

    public Socket mSocket;

    public void iniSocket() {

        if (bids.size() > 0) {
            bids.clear();
        }

        final ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);
        debugLog("joinRoom = " + prefs.getTOKEN() + "  :  " + publicSale.getIdLeilao());

        String server = ConnectionUrl.getUrlBaseSocket();
        MsgUtil.debugLog(server + " = Socket Url");
        try {
            mSocket = IO.socket(server);
        } catch (URISyntaxException ignored) {
            iniSocket();
        }

        mSocket.emit("joinRoom", prefs.getTOKEN(), publicSale.getIdLeilao());
        mSocket.on("currentBid", currentBid);
        mSocket.on("initRoom", initRoom);
        mSocket.on("newBid", newBid);
        mSocket.on("chamado", chamado);
        mSocket.on("cronometro", chronometer);
        mSocket.on("por_usuarios", perUsers);
        mSocket.on("dateStart", dateStart);
        mSocket.on("dateEnd", dateEnd);
        mSocket.on("winner", winner);
        checkSocketStatus();
        mSocket.connect();
    }

    //==============================================================================================
    //
    // Socket
    //
    //==============================================================================================

    private Emitter.Listener currentBid = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {

                    try{
                        if(args.length > 0){
                            String data = String.valueOf(args[0]);
                            currentValue = data;
                            debugLog("currentBid " + String.valueOf(data));
                            String[] valor = data.split(",");

                            valor_principal.setText(valor[0]);

                            if (valor[1].equals("null") || valor[1].equals("")) {
                                valor_secundario.setText("00");
                            } else {
                                valor_secundario.setText(valor[1]);
                            }
                        }
                    }catch(Exception e){

                    }
                }
            });
        }
    };

    private List<String> bids = new ArrayList<>();
    private Emitter.Listener initRoom = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    debugLog("initRoom " + data);

                    bids.add(data);
                    showBtnHistory();
                }
            });
        }

    };

    private Emitter.Listener newBid = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    debugLog("Last Bid " + data);
                    bids.add(data);
                    if (dialog_bids != null) {
                        addTextView(data);
                    }
                    showBtnHistory();
                    showLastBid(data);
                }
            });
        }

    };

    private String chrometerValue;
    private Emitter.Listener chronometer = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    chrometerValue = data;
                    debugLog("chronometer " + data);
                }
            });
        }

    };

    private Emitter.Listener perUsers = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    initPerUsers = data.equals("1");
                    started();
                    debugLog("perUsers " + data);
                }
            });
        }
    };


    private Emitter.Listener chamado = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                  //  chamados = data.equals("1");
                  //  started();
                    debugLog("chamados " + data);
                }
            });
        }
    };
    private Emitter.Listener dateStart = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    if(!data.equals("false")) {
                        try {
                            dataStartCountDown = Integer.parseInt(data);
                            started();
                            debugLog("dateStart " + data);
                        }catch (Exception e){
                            Log.e("TAG","Error",e);
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener dateEnd = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    dataEndCountDown = Integer.parseInt(data);
                    started();
                    debugLog("dateEnd " + data);
                }
            });
        }
    };

    private Emitter.Listener winner = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = String.valueOf(args[0]);
                    Log.w("Winner", data);
                    if(args.length > 1){
                        id_usuario = String.valueOf(args[1]);
                         id_leilao = String.valueOf(args[2]);
                    }

                    NotificationUtils notificationUtils = new  NotificationUtils(activity);

                    Map<String, String> map = new ArrayMap<>();
                    map.put(FB_NORTIFICATION_ID, "");
                    map.put(FB_NORTIFICATION_MENSAGEM,  "");
                    map.put(FB_NORTIFICATION_TITULO,  "");

                    notificationUtils.initNotification(activity, "Leilão 24h", data, map);
                  //  MsgUtil.Funcao_BottomSheetDialog(activity, data, clickListener_winner, null);

                    final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    @SuppressLint("InflateParams") final View dialogbox = activity.getLayoutInflater().inflate(R.layout.dialog_winner, null);

                    ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
                    TextView tv_title = dialogbox.findViewById(R.id.tv_title);
                    TextView tvBidLabel = dialogbox.findViewById(R.id.tvBidLabel);
                    Button winnerConfirm = dialogbox.findViewById(R.id.winnerConfirm);
                    Button winnerCancel = dialogbox.findViewById(R.id.winnerCancel);

                    builder.setView(dialogbox);
                    final Dialog dialog = builder.create();
                    Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    if(hasWindowFocus())
                        dialog.show();

                    tvBidLabel.setText(data);

                    winnerConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityUtil.callActivityWebView(activity, ConnectionUrl.getUrlPayment(id_usuario
                                    ,id_leilao), false);
                            functionFinish();
                            dialog.dismiss();
                        }
                    });

                    btn_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            functionFinish();
                            dialog.dismiss();
                        }
                    });

                    winnerCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            functionFinish();
                            dialog.dismiss();
                        }
                    });
                }
            });
        }
    };

    private View.OnClickListener clickListener_winner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ActivityUtil.callActivityWebView(activity, ConnectionUrl.getUrlPayment(id_usuario
                    ,id_leilao), false);
            functionFinish();
        }
    };

    //==============================================================================================
    //
    // Socket's Util
    //
    //==============================================================================================

    private void socketDisconect() {
        if (mSocket != null)
            mSocket.disconnect();
    }

    private void checkSocketStatus() {

        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_CONNECT", args);
            }
        });

        mSocket.on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_CONNECTING", args);
            }
        });

        mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_DISCONNECT", args);
            }
        });

        mSocket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_ERROR", args);
                iniSocket();
            }
        });

        mSocket.on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runResponse("EVENT_MESSAGE", args);
            }
        });


//        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_CONNECT_TIMEOUT", args);
//            }
//        });
//
//
//        mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_CONNECT_ERROR" , args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_PING, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_PING", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_PONG, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_PONG", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT_ATTEMPT, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT_ATTEMPT", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT_ERROR, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT_ERROR", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECTING", args);
//            }
//        });
//
//        mSocket.on(Socket.EVENT_RECONNECT_FAILED, new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                runResponse("EVENT_RECONNECT_FAILED", args);
//            }
//        });
    }

    private void runResponse(final String status, final Object[] args) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (args.length > 0) {
                    String data = String.valueOf(args[0]);
                    debugLog(status + " " + data);
                } else {
                    debugLog(status + " init");
                }
            }
        });
    }

    //==============================================================================================
    //
    // ** Modal Alert user for Bid
    //
    //==============================================================================================

    @SuppressLint("SetTextI18n")
    private void modalBid() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        @SuppressLint("InflateParams") final View dialogbox = activity.getLayoutInflater().inflate(R.layout.dialog_bid, null);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
        TextView tvBid = dialogbox.findViewById(R.id.tvBidLabel);
        Button bidConfirm = dialogbox.findViewById(R.id.bidConfirm);

        builder.setView(dialogbox);
        final Dialog dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Float value = Float.parseFloat(currentValue.replace(".", "").replace(",", ".")) + Float.parseFloat(currentBidValue);

        tvBid.setText("Confirmar lance de " + ToolBox_Mascaras.Funcao_Mascara_Moeda_NumberFormat(String.valueOf(value)));

        bidConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FunctionEmitBid();
                handle.removeCallbacksAndMessages(null);
                dialog.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    private void FunctionEmitBid() {
        mSocket.emit("bid"); // enviar
        ToolBox_RingTone.Funcao_Play(activity);
    }

    //==============================================================================================
    //
    // History List Bids from Socket
    //
    //==============================================================================================

    private void showBtnHistory() {
        if (bids.size() > 0) {
            btn_historico.setVisibility(View.VISIBLE);
            showLastBid(bids.get(bids.size() - 1));
        } else {
            btn_historico.setVisibility(GONE);
        }

        btn_historico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModalHistory();
            }
        });
    }

    private void showLastBid(String data) {
        ultimo_lance.setText(data);
        ultimo_lance.setVisibility(View.VISIBLE);
    }

    // ** Dialog History
    //==============================================================================================

    //Dialog
    private Dialog dialog_bids;
    private LinearLayout ll_itens_dialog;

    private void ModalHistory() {
        dialog_bids = new Dialog(activity);
        dialog_bids.setContentView(R.layout.dialog_historico);
        ll_itens_dialog = dialog_bids.findViewById(R.id.sv_itens);
        for (int i = 0; i < bids.size(); i++) {
            TextView textView = new TextView(activity);
            textView.setText(bids.get(i));

            ll_itens_dialog.addView(textView);
        }

        Objects.requireNonNull(dialog_bids.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog_bids.show();
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_bids.dismiss();
            }
        });

    }

    private void addTextView(String string) {
        TextView textView = new TextView(activity);
        textView.setText(string);
        if (ll_itens_dialog != null)
            ll_itens_dialog.addView(textView);
    }

    //==============================================================================================
    //
    // Util's page
    //
    //==============================================================================================

    @SuppressLint("SetTextI18n")
    public void started() {
        finaliza.setVisibility(View.VISIBLE);
        labelEndDate.setText("Fim do Leilão");

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (initPerUsers) {

                    tvLabelChonometer.setText("");
                    labelEndDate.setText("Aguardando usuários");
                    finaliza.setVisibility(GONE);
                    btn_dar_lance.setVisibility(View.VISIBLE);
                    cronometerFunctions(activity, conometro, Long.valueOf(chrometerValue), true);
                    btn_favorito.setVisibility(View.VISIBLE);
                } else {

                    if (dataStartCountDown > 0) {
                        tvLabelChonometer.setText("Inicia em:");
                        tvLabelChonometer.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                        finaliza.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(DATA_INICIO, false, false));
                        btn_dar_lance.setVisibility(GONE);
                        cronometerFunctions(activity, conometro, dataStartCountDown, false);
                        btn_favorito.setVisibility(View.VISIBLE);
                    } else if (dataEndCountDown > 0) {
                        tvLabelChonometer.setText("");
                        tvLabelChonometer.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                        finaliza.setText(ToolBox_Data_e_Hora.Funcao_Converter_hora_TimeStamp(DATA_FIM_PREVISTO, false, false));
                        btn_dar_lance.setVisibility(View.VISIBLE);
                        cronometerFunctions(activity, conometro, dataEndCountDown, false);
                        btn_favorito.setVisibility(View.VISIBLE);
                    } else {
                        publicSaleFinished();
                    }

                }

                ProjectUtils.verifyFinishPublicSale(activity,
                        tvLabelChonometer,
                        finaliza,
                        DATA_INICIO,
                        DATA_FIM_PREVISTO);

                closeLoading();
            }
        });
    }

    //==============================================================================================
    //
    // ** Public sale status page
    //
    //==============================================================================================

    private void publicSaleFinished() {
        tvLabelChonometer.setText("");
        labelEndDate.setText(getString(R.string.label_public_finished));
        labelEndDate.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        finaliza.setText("");
        conometro.setText("00:00:00");
        btn_dar_lance.setVisibility(GONE);
        btn_favorito.setVisibility(GONE);
    }

    //==============================================================================================
    //
    // ** Cronometer Function
    //
    //==============================================================================================
    private static CountDownTimer timer = null;

    private void cronometerFunctions(final Activity activity, final TextView textView, final Long data, final Boolean paused) {

        if (timer != null) {
            timer.cancel();
        }

        timer = new CountDownTimer(data * 1000, 1000) {

            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {

                Long day = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                Long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished));
                Long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                Long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));

                if (day >= 1) {
                    textView.setText("" + String.format("%02dD %02d:%02d:%02d",
                            day,
                            hours,
                            minutes,
                            seconds
                    ));
                } else if (hours >= 1) {
                    textView.setText("" + String.format("%02d:%02d:%02d",
                            hours,
                            minutes,
                            seconds
                    ));
                } else if (minutes >= 1) {
                    textView.setText("" + String.format("%02d:%02d",
                            minutes,
                            seconds
                    ));
                } else if (seconds >=1) {
                    textView.setText("" + String.format("%02d",
                            seconds
                    ));
                }


                if (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.DAYS.toSeconds(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)) <= 50) {
                    textView.setTextColor(activity.getResources().getColor(R.color.google_app_red));
                }

                if (paused) {
                    timer.cancel();
                }

            }

            public void onFinish() {
                textView.setText("" + String.format("%02d",
                        00
                ));

                handle.postDelayed(new Runnable() {
                    @Override public void run() {
                        mSocket.emit("bidEnd"); // enviar
                        // socketDisconect();
                        publicSaleFinished();
                    }
                }, 4000);

            }
        }.start();
    }

    //==============================================================================================
    //
    // ** App Name Simple Animation
    //
    //==============================================================================================

    private void showSmallAnimator() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1500);
        alphaAnimation.setStartOffset(1000);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tv_share_view_tip.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tv_share_view_tip.startAnimation(alphaAnimation);
    }

    //==============================================================================================
    //
    // Override
    //
    //==============================================================================================

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        functionFinish();
        super.onBackPressed();
    }

    public void functionFinish() {
        socketDisconect();
        if (loadding.getStatusDialog()) {
            loadding.closeLoading();
            finish();
        } else {
            finish();
        }
    }

    //==============================================================================================
    //
    // ** Connections Funtions
    //
    //==============================================================================================

    private void getPublicSaleFromWS(String idPublicSale) {
        connection.getPublicSale(idPublicSale);
    }

    //==============================================================================================
    //
    // Connection Vars
    //
    //==============================================================================================

    //connection
    private Connection connection;

    public void initWSConection() {
        setConnectionCallback();
        connection = new Connection(activity);
    }

    private void setConnectionCallback() {
        Connection.setCallback(PublicSale.this);
    }

    private void cashInCraditForPublicSaleEnter() {
        connection.chargeCreditFromRoom(publicSale.getIdLeilao());
    }

    //==============================================================================================
    //
    // ** Connection Callback For Notification Only
    //
    //==============================================================================================

    @Override
    public void ConnectionSuccess(WSResponse response, Connection.ConnectionType connectionTypeID) {


        ToolBox_SharedPrefs sharedPrefs = new ToolBox_SharedPrefs(activity);
        sharedPrefs.setCredits(response.getCreditos());
        sharedPrefs.setWhatsapp(sharedPrefs.getWhatsapp());


        if (connectionTypeID == Connection.ConnectionType.WS_ADDFAVORITE) {
            debugLog("Public Sale Added in Favorits");
        }

        if (connectionTypeID == Connection.ConnectionType.WS_CACHINCREDIT) {
            debugLog("Wellcome to " + publicSale.getNomeProduto() + " Public Sale, charge " + publicSale.getIngressos() + " to enter");

            if(response.getStatus() == STATUS_OK){
                iniSocket();
            }else if(response.getStatus() == STATUS_COMPRAR_INGRESSO){
                ProjectDialog.openMoreCredits(activity);
            }
        }

        if (connectionTypeID == Connection.ConnectionType.WS_PUBLICSALE) {
            debugLog("BOOM!!, Public sale data is here man!");
            if (response.getLeiloes().size() > 0) {
                startActivityByPublicSale(response.getLeiloes().get(0));
            } else {
                MsgUtil.Funcao_TOAST(activity, "Produto não encontrado");
                onBackPressed();
            }
        }

    }

    @Override
    public void ConnectionError(int statusType, WSResponse response, Connection.ConnectionType connectionTypeID) {

        if (connectionTypeID == Connection.ConnectionType.WS_ADDFAVORITE) {
            debugLog("Public Sale NOT Added in Favorits");
            updateFavoritStatusView();
        }

        if (connectionTypeID == Connection.ConnectionType.WS_CACHINCREDIT) {
            debugLog("Error to enter room " + publicSale.getNomeProduto() + " not change " + publicSale.getIngressos() + " to enter");
            finish();
        }

        if (connectionTypeID == Connection.ConnectionType.WS_PUBLICSALE) {
            debugLog("WOW!!, Public sale data is missing???!");
            MsgUtil.Funcao_TOAST(getApplicationContext(), "Leilão não encontrado");
            finish();
        }
    }

    @Override
    public void ConnectionFail(Throwable t) {
        finish();
    }
}
