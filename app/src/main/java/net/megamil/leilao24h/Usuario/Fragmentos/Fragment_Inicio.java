/*
 * Copyright (c) Developed by John Alves at 2018/10/31.
 */

package net.megamil.leilao24h.Usuario.Fragmentos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.quanticheart.core.bannerView.BannerView;
import com.quanticheart.core.bannerView.entity.Banner;

import net.megamil.leilao24h.BuildConfig;
import net.megamil.leilao24h.Connection.StatusUtils;
import net.megamil.leilao24h.Project.DialogPayment.DialogPayment;
import net.megamil.leilao24h.Project.ProjectActivity;
import net.megamil.leilao24h.R;
import net.megamil.leilao24h.Usuario.Adaptadores.AdapterCategory;
import net.megamil.leilao24h.Usuario.Adaptadores.AdapterPublicSale;
import net.megamil.leilao24h.Usuario.Fragmentos.Inicio.PublicSale;
import net.megamil.leilao24h.Usuario.Pagina_Inicio;
import net.megamil.leilao24h.Utils.ToolBox.MsgUtil;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs;
import net.megamil.leilao24h.Utils.ToolBox.ToolBox_SharedPrefs_Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import maripoppis.com.connection.Connection;
import maripoppis.com.connection.Model.SubModels.Leiloes;
import maripoppis.com.connection.Model.WSResponse;

public class Fragment_Inicio extends DebugFragment implements Connection.ConnectCallback, PublicSale.UpdateFavoriteStatus {

    //var padroes
    private Activity activity;
    public View view;

    //LinearLayout
    private LinearLayout btn_comprar_ingresso;

    //BannerView
    private BannerView bannerView;

    //textView
    @SuppressLint("StaticFieldLeak")
    public static TextView tv_credits;

    //SwipeRefreshLayout
    private SwipeRefreshLayout refreshLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_activity_inicio_inicio, container, false);

        activity = getActivity();

        initVars(view);
        initActions();
        initList();
        initRefresh();
        initWSConection();

        return view;
    }

    private void initVars(View view) {

        btn_comprar_ingresso = view.findViewById(R.id.btn_comprar_ingressos);

        recyclerView = view.findViewById(R.id.recycler_view_recycler_view);
        bannerView = view.findViewById(R.id.bannerView);
        refreshLayout = view.findViewById(R.id.swipe_refresh_layout_recycler_view);

        tv_credits = view.findViewById(R.id.tv_credits);
    }

    private void initActions() {

        updateCreditos();

        btn_comprar_ingresso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectActivity.goCreatePublicSaleActivity(activity);
            }
        });
    }

    private void updateCreditos() {
        ToolBox_SharedPrefs prefs = new ToolBox_SharedPrefs(activity);
        tv_credits.setText(prefs.getCredits());
    }

    //==============================================================================================
    //
    // ** Refresh
    //
    //==============================================================================================

    public void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                clearTempList();
                adapter.removeAll();
                getListPage(adapter.getFirstPagination());
            }
        });

    }

    private void hideRefresh() {
        refreshLayout.setRefreshing(false);
    }

    //==============================================================================================
    //
    // ** Create List
    //
    //==============================================================================================

    //RecyclerView
    private RecyclerView recyclerView;
    private AdapterPublicSale adapter;

    public void initList() {
        adapter = new AdapterPublicSale(
                activity,
                recyclerView,
                true
        );
        recyclerView.setAdapter(adapter);

        adapter.addPaginationScrollListener(new AdapterPublicSale.ScrollPaginationListener() {
            @Override
            public void scrollCallback(int pageNumber) {
                getListPage(pageNumber);
            }
        });

        adapter.setOnTryLoadListener(new AdapterPublicSale.OnTryLoadListener() {
            @Override
            public void tryLoad(int pageNumber) {
                getListPage(pageNumber);
            }
        });
    }

    //==============================================================================================
    //
    // ** Go Sale Activity
    //
    //==============================================================================================


    //==============================================================================================
    //
    // Connection Vars
    //
    //==============================================================================================

    //connection
    private Connection connection;

    public void initWSConection() {
        setConnectionCallback();
        connection = new Connection(getActivity());
//        connection.getPendencias();
        connection.getCategoty();
        verifyInitialData();
    }

    private void setConnectionCallback() {
        Connection.setCallback(Fragment_Inicio.this);
    }

    public void getListPage(int pageNumber) {
        setTempPaginationNumber(pageNumber);
        connection.getPublicSaleList(pageNumber);
    }

    // ** Virify Data TEMP
    //==============================================================================================

    private void verifyInitialData() {
        if (getTempList().size() > 0) {
            adapter.addItems(getTempList());
            adapter.setPaginationNumber(getTempPaginationNumber());
            MsgUtil.debugLog("TEMP List " + getTempList().size() + "");
        } else {
            getListPage(adapter.getFirstPagination());
            refreshLayout.setRefreshing(true);
        }
    }

    //==============================================================================================
    //
    // Connection Callback
    //
    //==============================================================================================

    @Override
    public void ConnectionSuccess(WSResponse response, Connection.ConnectionType connectionTypeID) {
        if (StatusUtils.verifyStatusConnection(activity, response, connectionTypeID)) {

            ToolBox_SharedPrefs sharedPrefs = new ToolBox_SharedPrefs(activity);
            sharedPrefs.setCredits(response.getCreditos());
            tv_credits.setText(sharedPrefs.getCredits());

            sharedPrefs.setWhatsapp(response.getWhatsapp());

            if (connectionTypeID == Connection.ConnectionType.WS_LISTPUBLICSALE) {
                addData(response.getLeiloes());
            }

            if (connectionTypeID == Connection.ConnectionType.WS_PENDENCIAS) {
                ToolBox_SharedPrefs_Usuario prefs = new ToolBox_SharedPrefs_Usuario(activity);
                DialogPayment.openDialogPayment(activity, response.getPendencias(), prefs.getID_USUARIO());
            }

            if (connectionTypeID == Connection.ConnectionType.WS_CATEGORIAS) {
                ArrayList<Banner> list = new ArrayList<>();
                for (int i = 0; i < response.getBanners().size(); i++) {
                    list.add(new Banner(BuildConfig.CONECTION_BASE_URL+"/upload/banners/" + response.getBanners().get(i).getBanner().trim(), null, null));
                }
                bannerView.setBannerList(list);
                if(getActivity() != null )
                new AdapterCategory(getActivity(), recyclerView).addItems(response.getCategorias());
            }
        }
        hideRefresh();
    }

    @Override
    public void ConnectionError(int statusType, WSResponse response, Connection.ConnectionType connectionTypeID) {
        StatusUtils.verifyStatusConnection(activity, response, connectionTypeID);
        hideRefresh();
    }

    @Override
    public void ConnectionFail(Throwable t) {
        hideRefresh();
    }

    // ** Utils
    //==============================================================================================
    public void addData(List<Leiloes> data) {
        if (data != null)
            setTempList(data);
        adapter.addItems(data);
    }

    //==============================================================================================
    //
    // ** @Override
    //
    //==============================================================================================

    @Override
    public void onResume() {
        super.onResume();
        PublicSale.setUpdateFavoriteStatus(Fragment_Inicio.this);
        setConnectionCallback();

        updateCreditos();
    }

    //==============================================================================================
    //
    // ** Temp Data
    //
    //==============================================================================================

    private void setTempList(List<Leiloes> data) {
        if (getActivity() != null)
            ((Pagina_Inicio) getActivity()).setDatabaseTemp(data);
    }

    private List<Leiloes> getTempList() {
        return ((Pagina_Inicio) Objects.requireNonNull(getActivity())).getDatabaseTemp();
    }

    private void clearTempList() {
        if (getActivity() != null)
            ((Pagina_Inicio) Objects.requireNonNull(getActivity())).clearDatabaseTemp();
    }

    private void setTempPaginationNumber(int paginationNumber) {
        if (getActivity() != null)
            ((Pagina_Inicio) Objects.requireNonNull(getActivity())).setPaginationNumberTemp(paginationNumber);
    }

    private int getTempPaginationNumber() {
        return ((Pagina_Inicio) Objects.requireNonNull(getActivity())).getPaginationNumberTemp();
    }

    //==============================================================================================
    //
    // ** Activity PublicSale Callback for new Favorite status
    //
    //==============================================================================================

    @Override
    public void favoriteStatus(String favoriteStatus, int position) {
        adapter.updateStatusFavorite(favoriteStatus, position);
    }

    //==============================================================================================
    //
    // ** Temp Data
    //
    //==============================================================================================

//    private void setTempList(List<Leiloes> data) {
//        adapter.setDatabaseTemp(data);
//    }
//
//    private List<Leiloes> getTempList() {
//        return adapter.getDatabaseTemp();
//    }
//
//    private void clearTempList() {
//        adapter.clearDatabaseTemp();
//    }
//
//    private void setTempPaginationNumber(int paginationNumber) {
//        adapter.setPaginationNumberTemp(paginationNumber);
//    }
//
//    private int getTempPaginationNumber() {
//        return adapter.getPaginationNumberTemp();
//    }
}
