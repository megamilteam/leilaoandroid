package net.megamil.library.Utils;/*
 * Copyright (c) Modifications and updates started on 2018/$today.mount/26, by programmer Jonn Alves, about the Naville Brasil contract.
 */

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.Window;

import net.megamil.library.R;

import java.util.Objects;

@SuppressWarnings("all")
public class DialogUtils {

    private static Dialog dialog;

    public static View getView(Activity activity, int layout) {
        return activity.getLayoutInflater().inflate(layout, null);
    }

    public static Dialog openDialogFullScreen(Activity activity, View layout) {
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(layout);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogNoAnimation;
        dialog.show();
        return dialog;
    }


    public static Dialog openDialogNoBackground(final Activity activity, View layout) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(layout);

        dialog = builder.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        return dialog;

    }

    public static void openDialog(final Activity activity, int layout) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final View dialogbox = activity.getLayoutInflater().inflate(layout, null);
        builder.setView(dialogbox);

        dialog = builder.create();
        dialog.show();

    }

    public static void openDialog(final Activity activity, View view) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();

    }
}
