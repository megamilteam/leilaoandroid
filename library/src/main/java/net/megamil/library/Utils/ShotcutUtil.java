package net.megamil.library.Utils;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;

import net.megamil.library.R;

import java.util.Arrays;
import java.util.Collections;

public class ShotcutUtil {

    //==============================================================================================
    //
    // ** init Vars
    //
    //==============================================================================================

    private static String shortcutID = "new_shortcut_01";
    private static String shortLabel = "Criar Leilão";
    private static String longLabel = "Criar novo leilão agora!";
    private static String shortcutDisabledMessage = "Este atalho so é liberado apos o login!";

//    private static int icon = R.mipmap.ic_launcher;

    //==============================================================================================
    //
    // ** Fixed Shortcut
    //
    //==============================================================================================

//    public static void createShotcut(Context context) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
//            shortcutManager(context, new ShortcutInfo.Builder(context, shortcutID)
//                    .setShortLabel(shortLabel)
//                    .setLongLabel(longLabel)
//                    .setDisabledMessage(shortcutDisabledMessage)
//
//                    .setIcon(Icon.createWithResource(context, icon))
//                    .setIntent(new Intent(Intent.ACTION_VIEW,
//                            Uri.parse("https://www.google.co.in")))
//                    .build());
//        }
//    }

    //==============================================================================================
    //
    // ** Dinamic Shortcut
    //
    //==============================================================================================

    public static void createShotcut(Context context,
                                     String shortcutID,
                                     String shortLabel,
                                     String longLabel,
                                     int icon,
                                     Intent intentAction) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            shortcutManager(context, new ShortcutInfo.Builder(context, shortcutID)
                    .setShortLabel(shortLabel)
                    .setLongLabel(longLabel)
                    .setIcon(Icon.createWithResource(context, icon))
                    .setIntent(intentAction)
                    .build());
        }
    }

    //==============================================================================================
    //
    // ** Create shortcutManager and add Shortcut
    //
    //==============================================================================================

    private static void shortcutManager(Context context, ShortcutInfo shortcut) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            shortcutManager.setDynamicShortcuts(Collections.singletonList(shortcut));
        }
    }

    //==============================================================================================
    //
    // ** create Dinamic Shortcut
    //
    //==============================================================================================

    public static void createDinamicShortcut(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ShortcutManager mShortcutManager =
                    context.getSystemService(ShortcutManager.class);

            if (mShortcutManager.isRequestPinShortcutSupported()) {
                // Assumes there's already a shortcut with the ID "my-shortcut".
                // The shortcut must be enabled.
                ShortcutInfo pinShortcutInfo =
                        new ShortcutInfo.Builder(context, "my-shortcut").build();

                // Create the PendingIntent object only if your app needs to be notified
                // that the user allowed the shortcut to be pinned. Note that, if the
                // pinning operation fails, your app isn't notified. We assume here that the
                // app has implemented a method called createShortcutResultIntent() that
                // returns a broadcast intent.
                Intent pinnedShortcutCallbackIntent =
                        mShortcutManager.createShortcutResultIntent(pinShortcutInfo);

                // Configure the intent so that your app's broadcast receiver gets
                // the callback successfully.For details, see PendingIntent.getBroadcast().
                PendingIntent successCallback = PendingIntent.getBroadcast(context, /* request code */ 0,
                        pinnedShortcutCallbackIntent, /* flags */ 0);

                mShortcutManager.requestPinShortcut(pinShortcutInfo,
                        successCallback.getIntentSender());
            }
        }

    }
}
