package net.megamil.library.Utils;/*
 * Copyright (c) Modifications and updates started on 2018/09/27, by programmer Jonn Alves, about the Naville Brasil contract.
 */


import android.app.Activity;
import android.content.Intent;

import net.megamil.library.ActivitySuport.IntentKeys;
import net.megamil.library.ActivitySuport.WebViewActivity;

/**
 * Created by John on 29/01/2018.
 */

public class ActivityUtil {

    /**
     * @param activity
     * @param intent
     * @param finish
     */
    public static void callActivity(Activity activity, final Intent intent, final Boolean finish) {
        initIntent(activity, verifieStatusIntent(intent), finish);
    }


    /**
     * @param activity
     * @param intentClass
     * @param finish
     */
    public static void callActivity(Activity activity, final Class intentClass, final Boolean finish) {
        initIntent(activity, verifieStatusIntent(new Intent(activity, intentClass)), finish);
    }

    /**
     * @param activity
     * @param url
     * @param finish
     */
    public static void callActivityWebView(Activity activity, String url, final Boolean finish) {
        WebViewActivity.callActivityWebView(activity,url,finish);
    }

    /**
     * @param activity
     * @param url
     * @param finish
     */
    public static void callActivityWebViewForTermOfUser(Activity activity, String url, final Boolean finish) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(IntentKeys.keyUrlForWebView, url);
        intent.putExtra(IntentKeys.keyWebViewForTerms, true);
        ActivityUtil.callActivity(activity, intent, finish);
    }

    /**
     * @param intent
     * @return
     */
    private static Intent verifieStatusIntent(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    /**
     * @param activity
     * @param intent
     * @param finish
     */
    private static void initIntent(Activity activity, Intent intent, boolean finish) {
        activity.startActivity(intent);
        if (finish) {
            activity.finish();
        }
    }


}
