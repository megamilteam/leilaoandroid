/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import maripoppis.com.connection.Constants.ConnectionUrl;
import maripoppis.com.connection.Database.Database;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class RestClient {

    private static Retrofit retrofit = null;

    static Retrofit getClient(Activity activity) {

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        final Database pref = new Database(activity);
        logDebug(pref.getTOKEN());
        client.addInterceptor( new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + pref.getTOKEN())
                        .build();
                return chain.proceed(newRequest);
            }
        });
//        }


        client.connectTimeout(20, TimeUnit.SECONDS);
        client.writeTimeout(20, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        if (BuildConfig.DEBUG) {
            // development build
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            // production build
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        client.addInterceptor(logging);
//        httpClient.addInterceptor(httpLoggingInterceptor);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ConnectionUrl.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
//                    .client(client)
                    .build();
        }
        return retrofit;
    }

    private static void logDebug(String Token) {
        if (BuildConfig.DEBUG) {
            Log.w("Conection Token", Token);
        }
    }

    private static Interceptor getInterceptor(final String token) {
        return new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        };
    }

}
