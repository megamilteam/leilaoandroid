/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection.Constants;

import maripoppis.com.connection.BuildConfig;

@SuppressWarnings("all")
public class ConnectionUrl {

    private static String SERVER = BuildConfig.CONECTION_BASE_URL;
    private static String PROJECT_NAME = BuildConfig.CONECTION_PROJECT_NAME;

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    private static String URL_BASE = SERVER + PROJECT_NAME;

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    private static String urlBaseSocket = SERVER + BuildConfig.CONECTION_SOCKET_PORT;
    private static String urlProjectController = URL_BASE + BuildConfig.CONECTION_CONTROLLER_WEBSERVICE;
    private static String urlProjectControllerPayment = URL_BASE + BuildConfig.CONECTION_CONTROLLER_PAYMENT;
    private static String urlBuyTicket = URL_BASE + "comprar_ingressos/";
    private static String urlPhotoProfile = URL_BASE + "upload/perfil/foto/";

    //==============================================================================================

    public static String getBaseUrl() {
        return getUrlProjectController();
    }

    //privacity Policy
    public static String getUrlPrivaciryPolicy() {
        return URL_BASE + "privacidade";
    }

    //Terms Of Users
    public static String getUrlTermOfUse() {
        return URL_BASE + "termos";
    }

    public static String getUrlBaseSocket() {
        return urlBaseSocket;
    }

    public static String getUrlProjectController() {
        return urlProjectController;
    }

    public static String getUrlBuyTicket() {
        return urlBuyTicket;
    }

    public static String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }

    //==============================================================================================
    //
    // ** Projets Make URL
    //
    //==============================================================================================

    public static String MakeUrlImage(String urlImage) {
        return SERVER + urlImage;
    }

    //==============================================================================================
    //
    // ** Utl for Payment
    //
    //==============================================================================================

    public static String getUrlPayment(String idUsuario, String idPublicSale) {
        return urlProjectControllerPayment + "newPreferenceProduct/" + idUsuario + "/" + idPublicSale;
    }

}
