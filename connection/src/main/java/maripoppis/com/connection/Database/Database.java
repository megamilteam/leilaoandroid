package maripoppis.com.connection.Database;

import android.content.Context;
import android.content.SharedPreferences;

public class Database {

    //==============================================================================================
    //
    // ** Create Database Shared
    //
    //==============================================================================================

    private final String DATABASE_NAME = "splash24h_us";
    private final String PREFIX = DATABASE_NAME;
    private final SharedPreferences Prefs;

    //==============================================================================================
    //
    // ** Get Prefs
    //
    //==============================================================================================

    private SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(DATABASE_NAME, Context.MODE_PRIVATE);
    }

    //==============================================================================================
    //
    // ** Contructor
    //
    //==============================================================================================

    public Database(Context context) {
        Prefs = getPrefs(context);
    }

    public Database(Context context, String token) {
        Prefs = getPrefs(context);
        setTOKEN(token);
    }

    //==============================================================================================
    //
    // ** Clear
    //
    //==============================================================================================
    public void clear() {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.clear();
        editor.apply();
    }

    //==============================================================================================
    //
    // ** Static Clear
    //
    //==============================================================================================

    public static void clear(Context context) {
        Database database = new Database(context);
        database.clear();
    }

    //==============================================================================================
    //
    // ** Getters and Setters
    //
    //==============================================================================================

    // ** Token - 25
    //==============================================================================================
    private final String DB_TOKEN = PREFIX + "25";

    public void setTOKEN(String string) {
        SharedPreferences.Editor editor = Prefs.edit();
        editor.putString(DB_TOKEN, string);
        editor.apply();
    }

    public String getTOKEN() {
        return Prefs.getString(DB_TOKEN, getDataNull());
    }

    //==============================================================================================
    //
    // ** Utils
    //
    //==============================================================================================

    public String getDataNull() {
        return "NotResult";
    }
}