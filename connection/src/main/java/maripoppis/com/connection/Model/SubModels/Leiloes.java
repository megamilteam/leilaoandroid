/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2018/11/4 at 6:29:6 for quantic heart studios
 *
 */

package maripoppis.com.connection.Model.SubModels;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Leiloes implements Serializable {

    @SerializedName("position")
    @Expose
    private int position;
    @SerializedName("id_leilao")
    @Expose
    private String idLeilao;
    @SerializedName("id_produto")
    @Expose
    private String idProduto;
    @SerializedName("nome_produto")
    @Expose
    private String nomeProduto;
    @SerializedName("descricao_produto")
    @Expose
    private String descricaoProduto;
    @SerializedName("valor_minimo")
    @Expose
    private String valorMinimo;
    @SerializedName("lance_minimo")
    @Expose
    private String lanceMinimo;
    @SerializedName("ingressos")
    @Expose
    private String ingressos;
    @SerializedName("data_inicio")
    @Expose
    private String dataInicio;
    @SerializedName("produto_usado")
    @Expose
    private String produtoUsado;

    @SerializedName("data_fim_previsto")
    @Expose
    private String dataFimPrevisto;
    @SerializedName("nome_usuario")
    @Expose
    private String nomeUsuario;
    @SerializedName("estado_usuario")
    @Expose
    private String estadoUsuario;

    @SerializedName("cidade_usuario")
    @Expose
    private String cidadeUsuario;
    @SerializedName("favorito")
    @Expose
    private String favorito;
    @SerializedName("avaliacao")
    @Expose
    private String avaliacao;
    @SerializedName("imagens")
    @Expose
    private List<String> imagens = null;

    //==============================================================================================
    //
    // **
    //
    //==============================================================================================
    public String getCidadeUsuario() {
        return cidadeUsuario;
    }

    public void setCidadeUsuario(String cidadeUsuario) {
        this.cidadeUsuario = cidadeUsuario;
    }
    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getIdLeilao() {
        return idLeilao;
    }

    public void setIdLeilao(String idLeilao) {
        this.idLeilao = idLeilao;
    }

    public String getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(String idProduto) {
        this.idProduto = idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public String getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(String valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public String getLanceMinimo() {
        return lanceMinimo;
    }

    public void setLanceMinimo(String lanceMinimo) {
        this.lanceMinimo = lanceMinimo;
    }

    public String getIngressos() {
        return ingressos;
    }

    public void setIngressos(String ingressos) {
        this.ingressos = ingressos;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getProdutoUsado() {
        return produtoUsado;
    }

    public void setProdutoUsado(String produtoUsado) {
        this.produtoUsado = produtoUsado;
    }

    public String getDataFimPrevisto() {
        return dataFimPrevisto;
    }

    public void setDataFimPrevisto(String dataFimPrevisto) {
        this.dataFimPrevisto = dataFimPrevisto;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getEstadoUsuario() {
        return estadoUsuario;
    }

    public void setEstadoUsuario(String estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }

    public String getFavorito() {
        return favorito;
    }

    public void setFavorito(String favorito) {
        this.favorito = favorito;
    }

    public String getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        this.avaliacao = avaliacao;
    }

    public List<String> getImagens() {
        return imagens;
    }

    public void setImagens(List<String> imagens) {
        this.imagens = imagens;
    }

    //==============================================================================================
    //
    // ** new ToString
    //
    //==============================================================================================

    @NonNull
    @Override
    public String toString() {
        return "Leiloes{" +
                "idLeilao='" + idLeilao + '\'' +
                ", idProduto='" + idProduto + '\'' +
                ", nomeProduto='" + nomeProduto + '\'' +
                ", descricaoProduto='" + descricaoProduto + '\'' +
                ", valorMinimo='" + valorMinimo + '\'' +
                ", lanceMinimo='" + lanceMinimo + '\'' +
                ", ingressos='" + ingressos + '\'' +
                ", dataInicio='" + dataInicio + '\'' +
                ", produtoUsado='" + produtoUsado + '\'' +
                ", dataFimPrevisto='" + dataFimPrevisto + '\'' +
                ", nomeUsuario='" + nomeUsuario + '\'' +
                ", estadoUsuario='" + estadoUsuario + '\'' +
                ", favorito='" + favorito + '\'' +
                ", avaliacao='" + avaliacao + '\'' +
                ", imagens=" + imagens +
                '}';
    }
}

