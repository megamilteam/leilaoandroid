/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/2/17 at 10:42:47 for quantic heart studios
 *
 */

package maripoppis.com.connection.Model.SubModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Produtos implements Serializable {

    @SerializedName("id_produto")
    @Expose
    private String idProduto;
    @SerializedName("nome_produto")
    @Expose
    private String nomeProduto;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("fk_categoria")
    @Expose
    private String fkCategoria;
    @SerializedName("produto_usado")
    @Expose
    private String produtoUsado;
    @SerializedName("imagens_array")
    @Expose
    private String imagensArray;
    @SerializedName("imagens")
    @Expose
    private String imagens;
    @SerializedName("descricao_produto")
    @Expose
    private String descricaoProduto;
    @SerializedName("nome_categoria")
    @Expose
    private String nomeCategoria;

    public String getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(String idProduto) {
        this.idProduto = idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFkCategoria() {
        return fkCategoria;
    }

    public void setFkCategoria(String fkCategoria) {
        this.fkCategoria = fkCategoria;
    }

    public String getProdutoUsado() {
        return produtoUsado;
    }

    public void setProdutoUsado(String produtoUsado) {
        this.produtoUsado = produtoUsado;
    }

    public String getImagensArray() {
        return imagensArray;
    }

    public void setImagensArray(String imagensArray) {
        this.imagensArray = imagensArray;
    }

    public String getImagens() {
        return imagens;
    }

    public void setImagens(String imagens) {
        this.imagens = imagens;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    @Override
    public String toString() {
        return "Produtos{" +
                "idProduto='" + idProduto + '\'' +
                ", nomeProduto='" + nomeProduto + '\'' +
                ", id='" + id + '\'' +
                ", fkCategoria='" + fkCategoria + '\'' +
                ", produtoUsado='" + produtoUsado + '\'' +
                ", imagensArray='" + imagensArray + '\'' +
                ", imagens='" + imagens + '\'' +
                ", descricaoProduto='" + descricaoProduto + '\'' +
                ", nomeCategoria='" + nomeCategoria + '\'' +
                '}';
    }
}