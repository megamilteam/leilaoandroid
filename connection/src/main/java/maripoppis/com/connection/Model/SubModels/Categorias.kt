/*
 *
 *  *                                     /@
 *  *                      __        __   /\/
 *  *                     /==\      /  \_/\/
 *  *                   /======\    \/\__ \__
 *  *                 /==/\  /\==\    /\_|__ \
 *  *              /==/    ||    \=\ / / / /_/
 *  *            /=/    /\ || /\   \=\/ /
 *  *         /===/   /   \||/   \   \===\
 *  *       /===/   /_________________ \===\
 *  *    /====/   / |                /  \====\
 *  *  /====/   /   |  _________    /      \===\
 *  *  /==/   /     | /   /  \ / / /         /===/
 *  * |===| /       |/   /____/ / /         /===/
 *  *  \==\             /\   / / /          /===/
 *  *  \===\__    \    /  \ / / /   /      /===/   ____                    __  _         __  __                __
 *  *    \==\ \    \\ /____/   /_\ //     /===/   / __ \__  ______  ____ _/ /_(_)____   / / / /__  ____ ______/ /_
 *  *    \===\ \   \\\\\\\/   ///////     /===/  / / / / / / / __ \/ __ `/ __/ / ___/  / /_/ / _ \/ __ `/ ___/ __/
 *  *      \==\/     \\\\/ / //////       /==/  / /_/ / /_/ / / / / /_/ / /_/ / /__   / __  /  __/ /_/ / /  / /_
 *  *      \==\     _ \\/ / /////        |==/   \___\_\__,_/_/ /_/\__,_/\__/_/\___/  /_/ /_/\___/\__,_/_/   \__/
 *  *        \==\  / \ / / ///          /===/
 *  *        \==\ /   / / /________/    /==/
 *  *          \==\  /               | /==/
 *  *          \=\  /________________|/=/
 *  *            \==\     _____     /==/
 *  *           / \===\   \   /   /===/
 *  *          / / /\===\  \_/  /===/
 *  *         / / /   \====\ /====/
 *  *        / / /      \===|===/
 *  *        |/_/         \===/
 *  *                       =
 *  *
 *  * Copyright(c) Developed by John Alves at 2019/10/8 at 0:39:38 for quantic heart studios
 *
 */

package maripoppis.com.connection.Model.SubModels

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Categorias {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("whatsapp")
    @Expose
    var whatsapp: String? = null
    @SerializedName("creditos")
    @Expose
    var creditos: String? = null
    @SerializedName("resultado")
    @Expose
    var resultado: String? = null
    @SerializedName("quantidade_por_pagina")
    @Expose
    var quantidadePorPagina: String? = null
    @SerializedName("pagina")
    @Expose
    var pagina: String? = null
    @SerializedName("leiloes")
    @Expose
    var leiloes: List<Any>? = null
    @SerializedName("categorias")
    @Expose
    var categorias: List<Categoria>? = null
    @SerializedName("banners")
    @Expose
    var banners: List<Banner>? = null

    inner class Banner {
        @SerializedName("banner")
        @Expose
        var banner: String? = null

        override fun toString(): String {
            return "Banner(banner=$banner)"
        }
    }

    inner class Categoria : Serializable {
        @SerializedName("id_categoria")
        @Expose
        var idCategoria: String? = null
        @SerializedName("icone")
        @Expose
        var icone: String? = null
        @SerializedName("nome_categoria")
        @Expose
        var nomeCategoria: String? = null
        @SerializedName("ativa")
        @Expose
        var ativa: String? = null
        @SerializedName("fk_categoria_pai")
        @Expose
        var fkCategoriaPai: String? = null
    }
}
