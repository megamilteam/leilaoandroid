
/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection.ConnectionUtils;

import android.util.Base64;

public class LoginUtils {

    /**
     * this method is API implemetation specific
     * might not work with other APIs
     **/
    public static String getAuthorizationHeader(String email, String password) {
        String credential = email + ":" + password;
        return "Basic " + Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
    }
//
//    public static void writeLogin(Activity activity, WSResponse body, boolean checkbox) {
//
//        if (checkbox) {
//            LoginBuilder lb = new LoginBuilder(activity);
//            lb.create(1, body.getDados().getUsuario().getEmail(), body.getDados().getUsuario().getSenha(), body.getDados().getUsuario().getToken());
//        }
//
//        Preferencias preferencias = new Preferencias(activity);
//        preferencias.salvarDados(body.getDados().getUsuario().getEmail(), body.getDados().getUsuario().getSenha(), body.getDados().getUsuario().getToken());
//
//        Intent intent = new Intent(activity, ApresentacaoActivity.class);
//        ActivityUtil.callActivity(activity, intent, true);
//
//    }
//
//    public static void callRegisterUser(Activity activity, Response<WSResponse> response) {
//
//        Intent intent = new Intent(activity, CadastroActivity.class);
//        intent.putExtra(CadastroActivity.keyEmailBunble, response.body().getDados().getUsuario().getEmail());
//        intent.putExtra(CadastroActivity.keyIdBunble, response.body().getDados().getUsuario().getId());
//        intent.putExtra(CadastroActivity.keyTypeBunble, response.body().getDados().getUsuario().getType());
//        ActivityUtil.callActivity(activity, intent, false);
//
//    }
//
//    public static void rawLogout() {
//
//        FacebookDataUtils.FacebookLogout();
//
//    }

}
