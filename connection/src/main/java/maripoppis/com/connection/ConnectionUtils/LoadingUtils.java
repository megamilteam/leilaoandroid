/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection.ConnectionUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import maripoppis.com.connection.Connection;
import maripoppis.com.connection.Model.WSResponse;
import maripoppis.com.connection.R;
import maripoppis.com.socialnetwork.FaceBook.FacebookAcountKitUtils;

public class LoadingUtils {

    //==============================================================================================
    //
    // ** Vars
    //
    //==============================================================================================

    private Activity mActivity;
    private Dialog dialog;
    private LinearLayout ll_load;
    private LinearLayout ll_erro;
    private TextView tv_erro;
    private Button btn_erro;
    private ImageView btn_close;

    //==============================================================================================
    //
    // ** Contructor
    //
    //==============================================================================================

    public LoadingUtils(Activity activity) {
        mActivity = activity;
    }

    //==============================================================================================
    //
    // ** Create Dialog view
    //
    //==============================================================================================

    public Dialog createDialogView(){
        dialog = new Dialog(mActivity, R.style.DialogFullscreen);
        View view = getView(mActivity, R.layout.activity_loading);

        ll_load = view.findViewById(R.id.ll_load);
        ll_erro = view.findViewById(R.id.ll_error);
        //
        tv_erro = view.findViewById(R.id.tv_error);
        //
        btn_erro = view.findViewById(R.id.btn_error);
        btn_close = view.findViewById(R.id.btn_close);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeLoading();
            }
        });

        ll_load.setVisibility(View.VISIBLE);

        dialog.setContentView(view);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogNoAnimation;
        dialog.setCancelable(false);
        return dialog;
    }

    //==============================================================================================
    //
    // ** Utils
    //
    //==============================================================================================

    private static View getView(Activity activity, int layout) {
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        return activity.getLayoutInflater().inflate(layout, null);
        return inflater.inflate(layout, null);
    }

    private Dialog openDialogFullScreen(Activity activity, View layout) {
        dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(layout);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogNoAnimation;
       if(dialog.isShowing())
        dialog.show();
        dialog.setCancelable(false);
        return dialog;
    }

    //==============================================================================================
    //
    // ** Utils
    //
    //==============================================================================================

    public void showLoading() {

        View view = getView(mActivity, R.layout.activity_loading);

        ll_load = view.findViewById(R.id.ll_load);
        ll_erro = view.findViewById(R.id.ll_error);
        //
        tv_erro = view.findViewById(R.id.tv_error);
        //
        btn_erro = view.findViewById(R.id.btn_error);
        btn_close = view.findViewById(R.id.btn_close);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeLoading();
            }
        });

        ll_load.setVisibility(View.VISIBLE);

        if (dialog == null) {
            dialog = openDialogFullScreen(mActivity, view);
        }

    }

    //==============================================================================================
    //
    //
    //
    //==============================================================================================

    public void closeLoading() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }


    public void closeLoading(final Activity activity, WSResponse response, Connection.ConnectionType wsTypeConection) {

        if (dialog != null) {
            ll_erro.setVisibility(View.VISIBLE);
            ll_load.setVisibility(View.GONE);

            switch (wsTypeConection) {
                case WS_LOGIN:
                    tv_erro.setText(response.getResultado());
                    btn_erro.setVisibility(View.GONE);
                    break;
            }
        }
    }


    public void closeLoadingByStatus(final Activity activity, String msg, int status) {

        if (dialog == null) {
            LoadingUtils utils = new LoadingUtils(activity);
            utils.showLoading();
            utils.closeLoadingByStatus(activity, msg, status);
        } else {

            ll_erro.setVisibility(View.VISIBLE);
            ll_load.setVisibility(View.GONE);

            switch (status) {
                case -3:
                    tv_erro.setText("Opa! Falta validar sua conta com seu número de celular!");
                    btn_erro.setText("Validar numero de Celular");
                    btn_erro.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FacebookAcountKitUtils.smsAtivation(activity);
                            closeLoading();
                        }
                    });
                    break;
                case -5:
                    tv_erro.setText(msg);
                    btn_erro.setText("OK");
                    btn_erro.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

//                            Intent intent = new Intent(activity, Editar_Usuario.class);
//
//                            try {
//                                JSONObject usuario = objeto.getJSONObject("usuarios");
//                                intent.putExtra("comp", usuario.toString());
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                            ToolBox_Chama_Activity.Funcao_Chama_Activity(activity, null, intent, false, false, "");
//                            closeDialog();

                            dialog.dismiss();
//                            {"status":-5,"resultado":"Para prosseguir por favor complete seu perfil","usuarios":{"id_usuario":"2665","nome_usuario":"Jonn alves","sexo_usuario":"","rg_usuario":"","cpf_usuario":"","celular_usuario":"11999999998","telefone_usuario":"","cep_usuario":"","logradouro_usuario":"","numero_usuario":"","bairro_usuario":"","cidade_usuario":"","estado_usuario":"","complemento_usuario":"","email_usuario":"jonny@test.com"}}
                        }
                    });
                    break;
            }
        }

    }


    public void openDialogWithStatus(String msg, String ButtonText, View.OnClickListener onClickListener) {
        ll_erro.setVisibility(View.VISIBLE);
        ll_load.setVisibility(View.GONE);
        tv_erro.setText(msg);
        btn_erro.setText(ButtonText);
        btn_erro.setOnClickListener(onClickListener);
        dialog.show();
    }

    public void openDialogInvalidToken(View.OnClickListener onClickListener) {

        ll_erro.setVisibility(View.VISIBLE);
        ll_load.setVisibility(View.GONE);
        btn_erro.setVisibility(View.GONE);
        btn_erro.setOnClickListener(onClickListener);

        final int raw_tm = Integer.parseInt(10 + "000");
        new CountDownTimer(raw_tm, 1000) {
            public void onTick(long millisUntilFinished) {
                tv_erro.setText(" Falha na autenticação de Usuário \nRedirecionando para o login em " + (millisUntilFinished / 1000));
            }
            public void onFinish() {
                btn_erro.performClick();
            }
        }.start();
        dialog.show();
    }

}


