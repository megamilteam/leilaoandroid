/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Objects;

import maripoppis.com.connection.ConnectionUtils.ConnectUtils;
import maripoppis.com.connection.ConnectionUtils.LoadingUtils;
import maripoppis.com.connection.ConnectionUtils.LoginUtils;
import maripoppis.com.connection.Constants.ConnectionContants;
import maripoppis.com.connection.Model.WSResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static maripoppis.com.connection.ConnectionUtils.ConnectUtils.logFunction;

public class Connection {

    //Loadding
    private LoadingUtils loading;

    //Log Help
    private String connectDescription = "";
    private static ConnectCallback callback;

    //Contructor
    private Activity activity;

    private static WSResponse.dados dados = null;

    //Retrofit
    private Api apiInterface;
    private Call<WSResponse> call = null;

    /**
     * @param mActivity call WS
     */
    public Connection(Activity mActivity) {
        activity = mActivity;
        apiInterface = RestClient.getClient(activity).create(Api.class);
        loading = new LoadingUtils(mActivity);
    }

    public enum ConnectionType {
        WS_LOGIN,
        WS_SMS_ACTIVATE,
        WS_SINGIN_USER,
        WS_RECOVERPASSWORD,
        WS_LISTPUBLICSALE,
        WS_PUBLICSALE,
        WS_CACHINCREDIT,
        WS_ADDFAVORITE,
        WS_PENDENCIAS,
        WS_CATEGORIAS,
        WS_CATEGORIAS_LEILOES
    }

    /**
     * @param connectionTypeID connection enum for
     * @param dados            for construct
     * @param showLoading      show Load?
     */
    public void getDataFrom(ConnectionType connectionTypeID, WSResponse.dados dados, Boolean showLoading) {
        Connection.dados = null;
        Connection.dados = dados;
        initWsConection(connectionTypeID);
    }

    /**
     * @param idPublicSale id public sale
     */
    public void getPublicSale(String idPublicSale) {
        call = apiInterface.getPublicSale(idPublicSale);
        call.enqueue(getEnquere(ConnectionType.WS_PUBLICSALE));
    }

    public void getCategoty() {
        call = apiInterface.getCategoryList(1, 200, -1, 1);
        call.enqueue(getEnquere(ConnectionType.WS_CATEGORIAS));
    }

    public void getPublicSaleByCategory(String fkCategory, int pageNumber) {
        Log.e("ID", fkCategory);
        call = apiInterface.getPublicSaleByCategory(fkCategory, 20, pageNumber);
        call.enqueue(getEnquere(ConnectionType.WS_CATEGORIAS_LEILOES));
    }

    /**
     * @param page number page load
     */
    public void getPublicSaleList(int page) {
        call = apiInterface.getPublicSaleList(page, ConnectionContants.NUMBER_PER_LOAD);
        call.enqueue(getEnquere(ConnectionType.WS_LISTPUBLICSALE));
    }

    /**
     * @param idPublicSale
     */
    public void setPublicSaleInFavorite(String idPublicSale) {
        call = apiInterface.setPublicSaleInFavorite(idPublicSale);
        call.enqueue(getEnquere(ConnectionType.WS_ADDFAVORITE));
    }

    /**
     * @param idPublicSale
     */
    public void chargeCreditFromRoom(String idPublicSale) {
        call = apiInterface.chargeCreditFromRoom(idPublicSale);
        call.enqueue(getEnquere(ConnectionType.WS_CACHINCREDIT));
    }

    public void getPendencias() {
        call = apiInterface.getPendencias();
        call.enqueue(getEnquere(ConnectionType.WS_PENDENCIAS));
    }

    public void goLogin(WSResponse.dados dados, Boolean showLoading) {
        apiInterface = RestClientLogin.getClient(activity).create(Api.class);
        Connection.dados = null;
        Connection.dados = dados;
        initWsConection(Connection.ConnectionType.WS_LOGIN);
    }

    public void goSmsValidation(WSResponse.dados dados, Boolean showLoading) {
        apiInterface = RestClientLogin.getClient(activity).create(Api.class);
        Connection.dados = null;
        Connection.dados = dados;
        initWsConection(Connection.ConnectionType.WS_SMS_ACTIVATE);
    }

    private void initWsConection(ConnectionType ID) {

        loading.showLoading();

        switch (ID) {
            case WS_LOGIN:

                if (dados.getLogin().getFacebook_usuario() == null) {
//                    call = apiInterface.setLogin(LoginUtils.getAuthorizationHeader(dados.getLogin().getUser(), dados.getLogin().getPass()), dados.getLogin());
                    call = apiInterface.setLogin(LoginUtils.getAuthorizationHeader(dados.getLogin().getUser(), dados.getLogin().getPass()), dados.getLogin().getToken());

                } else {
                    call = apiInterface.setLoginSocialNetwork(dados.getLogin());
                }
                break;
            case WS_SMS_ACTIVATE:
                call = apiInterface.smsActivate(dados.getLogin());
                break;


//            case WS_SINGIN_USER:
//                connectDescription = "Register Called";
//                call = apiInterface.createNewUser(dados.getUsuario());
//
//                break;
//
//            case WS_RECOVERPASSWORD:
//                connectDescription = "RecoverPass Called";
//                call = apiInterface.setRecoverPassword(dados.getUsuario());
//                break;

        }

        call.enqueue(getEnquere(ID));

    }

    private Callback<WSResponse> getEnquere(final ConnectionType connectionTypeID) {
        return new Callback<WSResponse>() {
            @Override
            public void onResponse(@NonNull Call<WSResponse> call, @NonNull Response<WSResponse> response) {
//                logFunction(response, connectDescription);
                //
                try{
                    if (response.isSuccessful()) {// Return in 200 at 300 http status
                        int status = verifieStatus(response, connectionTypeID);
                        if (status == STATUS_OK || status == STATUS_PERFIL_NOT_FULL || status == STATUS_INVALID_TOKEN || status == STATUS_COMPRAR_INGRESSO) {
                            callback.ConnectionSuccess(response.body(), connectionTypeID);
                            loading.closeLoading();
                        }
                    } else if (response.code() >= 400) {
                        WSResponse wsResponse = getErrorBody(Objects.requireNonNull(response.errorBody()));

                        ConnectUtils.logW("Status", wsResponse.getResultado());
                        callback.ConnectionError(STATUS_FAIL, wsResponse, connectionTypeID);
                        loading.closeLoading(activity, wsResponse, ConnectionType.WS_LOGIN);
                    }
                }catch (Exception e){
                    Log.e("Error","Conectividade: ",e);

                }

                // ConnectUtils.hideSimpleLoadingDialog();
            }

            @Override
            public void onFailure(@NonNull Call<WSResponse> call, @NonNull Throwable t) {
                logFunction(t, connectDescription);
                callback.ConnectionFail(t);
//                ConnectUtils.hideSimpleLoadingDialog();
            }
        };
    }

    private int verifieStatus(Response<WSResponse> response, ConnectionType connectionTypeID) {
        int statusWS = Objects.requireNonNull(response.body()).getStatus();
        switch (statusWS) {
            case 0:
                if (dados.getLogin().getFacebook_usuario() != null) {
                    callback.ConnectionError(STATUS_NEW_FACEBOOK_USER, response.body(), connectionTypeID);
                    loading.closeLoading();
                }
                break;
            case -3:
                loading.closeLoadingByStatus(activity, response.body().getResultado(), statusWS);
                callback.ConnectionError(STATUS_SMS_ACTIVATION, response.body(), connectionTypeID);
                break;
            case -5:
//                loading.closeLoadingByStatus(activity, response.body().getResultado(), statusWS);
//                callback.ConnectionError(STATUS_PERFIL_NOT_FULL, response.body(), connectionTypeID);
                break;
            default:
                loading.closeLoading(activity, response.body(), connectionTypeID);
//                callback.ConnectionError(STATUS_FAIL, response.body(), connectionTypeID);
                break;
        }
        return statusWS;
    }


    //Utils
    // =============================================================================================

    /**
     * @param responseBody error body for Gson
     * @return WSResponse
     */
    private WSResponse getErrorBody(ResponseBody responseBody) {
        Gson gson = new GsonBuilder().create();
        try {
            return gson.fromJson(responseBody.string(), WSResponse.class);
        } catch (IOException e) {
            return null;
        }
    }

    //Interface
    // =============================================================================================

    //Status Return WS
    public static int STATUS_FAIL = 0;
    public static int STATUS_OK = 1;

    //Others Status
    public static int STATUS_SMS_ACTIVATION = -3;
    public static int STATUS_INVALID_TOKEN = -4;
    public static int STATUS_PERFIL_NOT_FULL = -5;
    public static int STATUS_NEW_FACEBOOK_USER = -10;
    public static int STATUS_COMPRAR_INGRESSO = -6;

    public static void setCallback(ConnectCallback mCallback) {
        callback = mCallback;
    }

    public interface ConnectCallback {

        void ConnectionSuccess(WSResponse response, ConnectionType connectionTypeID);

        void ConnectionError(int statusType, WSResponse response, ConnectionType connectionTypeID);

        void ConnectionFail(Throwable t);

    }

}
