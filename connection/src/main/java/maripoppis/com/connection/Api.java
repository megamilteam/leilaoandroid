/*
 * Copyright (c) Developed by John Alves at 2018/10/29.
 */

package maripoppis.com.connection;

import maripoppis.com.connection.Model.SubModels.Login;
import maripoppis.com.connection.Model.WSResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    //Login, Recover Password and Sing In
//    @POST("login_usuario")
//    Call<WSResponse> setLogin(@Header("Authorization") String authKey, @Body Login login);

    @POST("login_usuario")
    @FormUrlEncoded
    Call<WSResponse> setLogin(@Header("Authorization") String authKey, @Field("token") String token);


    @POST("login_usuario")
    Call<WSResponse> setLoginSocialNetwork(@Body Login login);

    @POST("ativar_sms")
    Call<WSResponse> smsActivate(@Body Login login);

//    @GET(newWs + "wsMagazines?limit=10")
//    Call<WSResponse> getMagazines(@Query("page") int wsPage);
//    //
//    @POST(newWs + "wsCreateUser")
//    Call<WSResponse> createNewUser(@Body Usuario usuarios);
//
//    @POST(newWs + "wsForgotPassword")
//    Call<WSResponse> setRecoverPassword(@Body Usuario usuarios);

    //Inside in app!


    //==============================================================================================
    //
    // ** Connections
    //
    //==============================================================================================
    @GET("listar_leiloes_ativos")
    Call<WSResponse> getPublicSale(@Query("id_leilao") String idPublicSale);

    @GET("pendencias")
    Call<WSResponse> getPendencias();

    @GET("leiloes_categorias")
    Call<WSResponse> getCategoryList(@Query("pagina") int page, @Query("quantidade_por_pagina") int quant, @Query("categoria") int categoria, @Query("todasCategorias") int todasCategorias);

    @GET("leiloes_categorias")
    Call<WSResponse> getPublicSaleByCategory(@Query("categoria") String id, @Query("quantidade_por_pagina") int quant, @Query("pagina") int pagina);

    @GET("listar_leiloes_ativos")
    Call<WSResponse> getPublicSaleList(@Query("pagina") int page, @Query("quantidade_por_pagina") int quant);

    @POST("favorito")
    @FormUrlEncoded
    Call<WSResponse> setPublicSaleInFavorite(@Field("id_leilao") String title);

    @POST("entrar_leilao")
    @FormUrlEncoded
    Call<WSResponse> chargeCreditFromRoom(@Field("id_leilao") String title);
}

